// console.log('process.env.NODE_ENV : ', process.env.NODE_ENV)

const isDevMode = process.env.NODE_ENV === 'development'

const esRules = {
  semi: 'error',
  // 'array-element-newline': [
  //   'error',
  //   {
  //     minItems: 1
  //   }
  // ],
  'vue/html-self-closing': 'off',
  'vue/no-multiple-template-root': 'off',
  'vue/multi-word-component-names': ['off']
}
if (isDevMode) {
  esRules['no-console'] = 'off'
}

module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true
  },
  extends: [
    '@nuxtjs/eslint-config-typescript',
    //'plugin:prettier/recommended',
    'plugin:vue/vue3-recommended',
    'prettier'
  ],
  ignorePatterns: ['_archives/*'],
  overrides: [
    {
      env: {
        node: true
      },
      files: ['.eslintrc.{js,cjs}'],
      parserOptions: {
        sourceType: 'script'
      }
      // extends: ["plugin:cypress/recommended"],
    }
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    parser: '@typescript-eslint/parser',
    sourceType: 'module'
  },
  plugins: ['@typescript-eslint', 'vue'],
  rules: esRules,
  ignorePatterns: [
    'dist/*', //
    'images/*',
    'npm-debug.log',
    '.env',
    '.env.*',
    'tsconfig.json',
    '.vscode/*',
    'README.md',
    '.eslintrc.cjs',
    '_archives/*'
  ]
}
