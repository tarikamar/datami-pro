<div style="text-align: center;">
  <img style="height: 50px" src="public/logo-DATAMI-rect-colors-03.png"/>
</div>

# Datami PRO - back office

**A backoffice for Datami publishers**

Built with :

- [Nuxt3](https://nuxt.com/docs/getting-started/introduction) : JS web framework (SSR)
- [TypeScript](https://nuxt.com/docs/guide/concepts/typescript) : JS types
- [Eslint](https://nuxt.com/modules/eslint) + [Prettier](https://prettier.io/) : linting
- [Vuetify](https://vuetifyjs.com/en/) and [Nuxt3 module](https://nuxt.com/modules/vuetify) : UI framework
- [i18n Nuxt3 module](https://nuxt.com/modules/i18n): internationalization
- [PostgreSQL](https://www.postgresql.org/) : database
- [Prisma](https://www.prisma.io/) : JS DB adapter
- [Vee-validate for Vue3](https://vee-validate.logaretm.com) and [Zod](https://vee-validate.logaretm.com/v4/integrations/zod-schema-validation/) : form inputs and schema validator
- [Pinia](https://pinia.vuejs.org/) : reactive store
- [Next-auth](https://next-auth.js.org/) and [@sidebase/nuxt-auth](https://sidebase.io/nuxt-auth/getting-started) : passwordless authentication with Oauth
- [Nodemailer](https://nodemailer.com/smtp/) : sending emails
- [Stripe](https://stripe.com/docs/api): payments API and service
- [Ngrok](https://github.com/ngrok/ngrok-javascript) : for tunneling & receiving webhooks locally
- [Xlsx](https://github.com/SheetJS/sheetjs) : for reading XLS files
- [VueDraggable](https://github.com/SortableJS/vue.draggable.next) : to be able to drag vue elements in a page

---

References :

- [Datami](https://datami.multi.coop)

---

## Setup

Make sure to install the dependencies :

```bash
# use current version of
nvm use

# Install dependencies
npm install

# Install ngrok on your machine
brew install ngrok

# Create local .env file from example
cp .env.example .env
```

You will need to update your `.env` file. The variables are the following :

```sh
# APP
APP_NAME="Datami PRO"
NODE_ENV="development"
AUTH_SECRET="my-auth-secret"
AUTH_ORIGIN="http://localhost:3000"
APP_DOMAIN="http://localhost:3000"

# APP API
## Note: you can use the following command line to generate a secret :
## node -e "console.log(crypto.randomBytes(32).toString('hex'))"
API_ROUTE_SECRET="my-route-secret"

# DATABASES
DATABASE_URL="postgresql://USER:PASSWORD@localhost:PORT/DB_NAME?schema=public"

# GITLAB
GITLAB_CLIENT_ID="my-gitlab-client-id"
GITLAB_CLIENT_SECRET=m"y-gitlab-client-secret"
GITLAB_SUDO_EMAIL="datami@multi.coop"
GITLAB_SUDO_PWD="123"
GITLAB_SUDO_TOKEN="123"

# GITHUB
GITHUB_CLIENT_ID="my-github-client-id"
GITHUB_CLIENT_SECRET="my-github-client-secret"
GITHUB_SUDO_EMAIL="datami@multi.coop"
GITHUB_SUDO_PWD="123"
GITHUB_SUDO_TOKEN="123"

# EMAILING
EMAIL_SERVER_USER="apikey"
EMAIL_SERVER_PASSWORD="<passowrd>"
EMAIL_SERVER_HOST="smtp.sendgrid.net"
EMAIL_SERVER_PORT="587"
EMAIL_FROM="<email>"

# STRIPE
STRIPE_PUBLISHABLE_KEY="pk_test_XXX"
STRIPE_SECRET_KEY="sk_test_XXX"

# NGROK
# NGROK_ACTIVATE="true"
# NGROK_DOMAIN="test-datami-pro"
# NGROK_AUTHTOKEN="my-ngrok-auth-token"
# NGROK_BASIC_AUTH="user:12345678"
```

---

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# Usual dev server
npm run dev
```

---

## Production

Build the application for production :

```bash
# Build app
npm run build
```

Locally preview production build :

```bash
# Preview build
npm run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.

---

## Tunneling (for webhooks listeners)

The app uses [Ngrok](https://ngrok.com) to be able to receive webhooks from online services such as Stripe. (You need an account).

Open a new shell and run :

### A) Random URL

```bash
# Starts a tunnel with a random sub-domain
ngrok http http://localhost:3000
```

=> Retieves a random url such as :

``

### B) Edge (stable url)

Create an [edge endpoint in Ngrok](https://dashboard.ngrok.com/cloud-edge/edges), go to [`Domains`](https://dashboard.ngrok.com/cloud-edge/domains), attach the Edge to your domain, and run :

```bash
# Starts a tunnel
ngrok tunnel --label edge=<edghts_XXX> http://localhost:3000
```

=> Retieves a random (but stable) url such as :

`https://touching-crisp-horse.ngrok-free.app/`

**Helpers on MacOS**

```bash
# List services running on port 3000
lsof -i tcp:3000

# List all running services
top

# Get pid of a ngrok service
brew install pidof
pidof ngrok
```

---

## Database

```bash
# Sync DB with schema
npm run prisma:migrate:dev

# Migration
npm run prisma:migrate:generate
```

```bash
# Prisma studio

npm run prisma:studio
# Prisma studio server on : http://localhost:5555
```
