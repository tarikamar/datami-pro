import { type ExternalLink } from '@/utils'

export const DatamiSponsors: ExternalLink[] = [
  // {
  //   title: 'Multi',
  //   subtitle: 'partners.sponsors.multi',
  //   url: 'https://multi.coop',
  //   logo: '/logos/multi-logo.png'
  // },
  {
    title: 'Europe',
    subtitle: 'partners.sponsors.europe',
    url: 'https://next-generation-eu.europa.eu/index_en',
    logo: '/sponsors/nextgen-eu-logo.png'
  },
  {
    title: 'NGI Enrichers',
    subtitle: 'partners.sponsors.ngienrichers',
    url: 'https://enrichers.ngi.eu/',
    logo: '/sponsors/ngi-enrichers-logo.png'
  },
  {
    title: 'ANCT',
    subtitle: 'partners.sponsors.anct',
    url: 'https://agence-cohesion-territoires.gouv.fr/',
    logo: '/sponsors/anct-logo.png'
  },
  // {
  //   title: 'ANCT',
  //   subtitle: 'partners.sponsors.bdt',
  //   url: 'https://agence-cohesion-territoires.gouv.fr/',
  //   logo: '/clients/anct-logo.png'
  // },
  {
    title: 'France Relance',
    subtitle: 'partners.sponsors.francerelance',
    url: 'https://www.economie.gouv.fr/plan-de-relance',
    logo: '/sponsors/france-relance-logo.jpg'
  }
]

export const DatamiClients: ExternalLink[] = [
  {
    title: 'Opendata France',
    subtitle: 'partners.clients.opendatafrance',
    url: 'https://www.opendatafrance.net/',
    logo: '/clients/odf-logo.svg'
  },
  {
    title: 'ADEME',
    subtitle: 'partners.clients.ademe',
    url: 'https://www.ademe.fr/',
    logo: '/clients/ademe-logo.png'
  },
  {
    title: 'FabMob',
    subtitle: 'partners.clients.fabmob',
    url: 'https://lafabriquedesmobilites.fr/',
    logo: '/clients/fabmob-logo.png'
  },
  {
    title: 'Mednum',
    subtitle: 'partners.clients.mednum',
    url: 'https://lamednum.coop/',
    logo: '/clients/mednum-logo.png'
  },
  {
    title: 'PiNG',
    subtitle: 'partners.clients.ping',
    url: 'https://www.pingbase.net/',
    logo: '/clients/ping-logo.png'
  },
  {
    title: 'CONUMM',
    subtitle: 'partners.clients.connum',
    url: 'https://www.pingbase.net/',
    logo: '/clients/conumm-logo.png'
  },
  {
    title: 'Rhinocc',
    subtitle: 'partners.clients.rhinocc',
    url: 'https://rhinocc.fr/',
    logo: '/clients/rhinocc-logo.jpg'
  },
  {
    title: 'Département du Doubs',
    subtitle: 'partners.clients.departementdoubs',
    url: 'https://www.doubs.fr/',
    logo: '/clients/dept-doubs-logo.jpg'
  },
  {
    title: "Francil'in",
    subtitle: 'partners.clients.francilin',
    url: 'https://carto.francilin.fr/',
    logo: '/clients/francilin-logo.jpg'
  },
  {
    title: 'Décider Ensemble',
    subtitle: 'partners.clients.deciderensemble',
    url: 'https://www.deciderensemble.com/',
    logo: '/clients/decider-ensemble-logo.jpg'
  },
  {
    title: 'Obsarm',
    subtitle: 'partners.clients.obsarm',
    url: 'https://www.obsarm.info/',
    logo: '/clients/obsarm-logo-solo.jpeg'
  }
]

export const footerLinks: ExternalLink[] = [
  {
    title: 'Multi-Element',
    subtitle: 'multi-element',
    url: 'https://app.element.io/#/room/#multi:multi.coop',
    icon: 'mdi-message-text-outline'
  },
  {
    title: 'Multi-Twitter',
    subtitle: 'multi-twitter',
    url: 'https://twitter.com/multi_coop',
    icon: 'mdi-twitter'
  },
  {
    title: 'Multi-Linkedin',
    subtitle: 'multi-linkedin',
    url: 'https://www.linkedin.com/company/11200772',
    icon: 'mdi-linkedin'
  },
  {
    title: 'Multi-Gitlab',
    subtitle: 'multi-gitlab',
    url: 'https://gitlab.com/multi-coop',
    icon: 'mdi-gitlab'
  },
  {
    title: 'Multi-Github',
    subtitle: 'multi-github',
    url: 'https://github.com/multi-coop',
    icon: 'mdi-github'
  }
]
