import {
  Currencies, //
  SubscriptionCategory,
  OfferFeatureCode,
  type Offer,
  type CurrencyBasic
} from '~/utils'

export const acceptedCurrrencies: CurrencyBasic[] = [
  { currency: Currencies.EUR, isDefault: true }, //
  { currency: Currencies.USD },
  { currency: Currencies.CAD }
]

export const datamiProducts: Offer[] = [
  // FREE PLAN
  {
    title: 'offers.free.title',
    subtitle: 'offers.free.subtitle',
    features: [
      {
        title: 'offer.feature.users',
        amount: 1,
        featCode: OfferFeatureCode.N_USERS
      },
      {
        title: 'offer.feature.datasets',
        amount: 5,
        featCode: OfferFeatureCode.N_DATASETS
      },
      {
        title: 'offer.feature.widgets',
        amount: 1,
        featCode: OfferFeatureCode.N_WIDGETS
      }
    ],
    stripeProductKey: 'prod_PRxlhhwS270grR',
    subscriptionCategory: SubscriptionCategory.FREE,
    prices: [
      { currency: Currencies.EUR, default: 1, stripePrice: undefined, stripePriceKey: 'price_1Od3pcD7Q8aPEphFnnAcX97E' },
      { currency: Currencies.USD, default: 1, stripePrice: undefined, stripePriceKey: 'price_1Od5AnD7Q8aPEphFVm6pLqsx' },
      { currency: Currencies.CAD, default: 1, stripePrice: undefined, stripePriceKey: 'price_1Od5AzD7Q8aPEphFULiIKcSu' }
    ]
  },

  // BASIC PLAN
  {
    title: 'offers.basic.title',
    disable: true,
    subtitle: 'offers.basic.subtitle',
    features: [
      {
        title: 'offer.feature.users',
        amount: 1,
        featCode: OfferFeatureCode.N_USERS
      },
      {
        title: 'offer.feature.datasets',
        amount: 5,
        featCode: OfferFeatureCode.N_DATASETS
      },
      {
        title: 'offer.feature.widgets',
        amount: 1,
        featCode: OfferFeatureCode.N_WIDGETS
      }
    ],
    stripeProductKey: 'prod_PSDdhtnRX0M7kZ',
    subscriptionCategory: SubscriptionCategory.FREE,
    prices: [
      { currency: Currencies.EUR, default: 1, stripePrice: undefined, stripePriceKey: 'price_1OdJC1D7Q8aPEphFmkRu1mTu' },
      { currency: Currencies.USD, default: 1, stripePrice: undefined, stripePriceKey: 'price_1OdJCWD7Q8aPEphFZMgdnIPd' },
      { currency: Currencies.CAD, default: 1, stripePrice: undefined, stripePriceKey: 'price_1OdJCWD7Q8aPEphFzc3xoBWZ' }
    ]
  },

  // PERSONAL PLAN
  {
    title: 'offers.personal.title',
    subtitle: 'offers.personal.subtitle',
    features: [
      {
        title: 'offer.feature.users',
        amount: 1,
        featCode: OfferFeatureCode.N_USERS
      },
      {
        title: 'offer.feature.datasets',
        amount: 50,
        featCode: OfferFeatureCode.N_DATASETS
      },
      {
        title: 'offer.feature.widgets',
        amount: 10,
        featCode: OfferFeatureCode.N_WIDGETS
      }
    ],
    stripeProductKey: 'prod_PRxeQPCHvr1ZOh',
    subscriptionCategory: SubscriptionCategory.PERSONAL,
    prices: [
      { currency: Currencies.EUR, default: 5, stripePrice: undefined, stripePriceKey: 'price_1Od3jOD7Q8aPEphF0f3IKegB' },
      { currency: Currencies.USD, default: 5, stripePrice: undefined, stripePriceKey: 'price_1Od4aqD7Q8aPEphF6Fcl8Tpw' },
      { currency: Currencies.CAD, default: 10, stripePrice: undefined, stripePriceKey: 'price_1Od4aqD7Q8aPEphF4STK1IHI' }
    ]
  },

  // PROFESSIONAL
  {
    title: 'offers.professional.title',
    subtitle: 'offers.professional.subtitle',
    features: [
      {
        title: 'offer.feature.users',
        amount: 10,
        featCode: OfferFeatureCode.N_USERS
      },
      {
        title: 'offer.feature.datasets',
        amount: 100,
        featCode: OfferFeatureCode.N_DATASETS
      },
      {
        title: 'offer.feature.widgets',
        amount: 50,
        featCode: OfferFeatureCode.N_WIDGETS
      }
    ],
    stripeProductKey: 'prod_PRxhSmkkd3xIbp',
    subscriptionCategory: SubscriptionCategory.PROFESSIONAL,
    prices: [
      { currency: Currencies.EUR, default: 20, stripePrice: undefined, stripePriceKey: 'price_1Od3m7D7Q8aPEphF6bSO36ES' },
      { currency: Currencies.USD, default: 20, stripePrice: undefined, stripePriceKey: 'price_1Od4ZfD7Q8aPEphFwHTOVzhm' },
      { currency: Currencies.CAD, default: 42, stripePrice: undefined, stripePriceKey: 'price_1Od4ZfD7Q8aPEphF4O7GQUgt' }
    ]
  },

  // PREMIUM
  {
    title: 'offers.premium.title',
    subtitle: 'offers.premium.subtitle',
    features: [
      {
        title: 'offer.feature.users',
        amount: 25,
        featCode: OfferFeatureCode.N_USERS
      },
      {
        title: 'offer.feature.datasets',
        amount: 500,
        featCode: OfferFeatureCode.N_DATASETS
      },
      {
        title: 'offer.feature.widgets',
        amount: 250,
        featCode: OfferFeatureCode.N_WIDGETS
      }
    ]
  }
]
