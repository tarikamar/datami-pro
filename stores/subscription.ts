import type { Stripe } from 'stripe'
import { acceptedCurrrencies, datamiProducts } from '~/data/products'
import type { StripeProductPrices } from '@/utils'
import { Currencies } from '@/utils'

export const useSubscriptionStore = defineStore('subscription', () => {
  // CURRENCIES
  const getCurrenciesCodes = computed(() => {
    return acceptedCurrrencies.map((currency) => currency.currency)
  })
  const getDefaultCurrency = computed(() => {
    return acceptedCurrrencies.find((currency) => currency.isDefault) || acceptedCurrrencies[0]
  })

  // PRODUCTS AND OFFERS
  const offers = ref<Offer[]>(datamiProducts)

  const activeOffers = computed(() => offers.value.filter((offer) => !offer.disable))

  const getProductsKeys = computed(() => {
    return offers.value.map((offer) => offer.stripeProductKey) as string[]
  })

  const getBodyProductsPrices = computed(() => {
    const products = offers.value.map((offer) => {
      const product = {
        productKey: offer.stripeProductKey,
        priceKeys: offer.prices?.map((price) => price.stripePriceKey)
      }
      return product
    })
    return products.filter((p) => p.priceKeys)
  })

  const getProductIndexByKey = (key: string) => {
    return offers.value.findIndex((offer) => offer.stripeProductKey === key)
  }

  // Mutations
  const setProductPrices = (productInfos: StripeProductPrices) => {
    // console.log('productInfos : ', productInfos)

    const index = getProductIndexByKey(productInfos.productKey)
    // console.log('index : ', index)

    const prevOffer = offers.value[index]
    // console.log('prevOffer : ', prevOffer)

    const updatedPrices: StripePrices[] = productInfos.prices.map((infos: Stripe.Price) => {
      const prevPrice = prevOffer.prices?.find((p) => p.stripePriceKey === infos.id)

      return {
        currency: infos.currency as Currencies,
        default: prevPrice?.default,
        stripePriceKey: infos.id,
        stripePrice: infos.unit_amount ? infos.unit_amount / 100 : undefined
      }
    })
    // console.log('updatedPrices: ', updatedPrices)

    prevOffer.prices = updatedPrices

    offers.value[index] = prevOffer
  }

  return {
    acceptedCurrrencies,
    getCurrenciesCodes,
    getDefaultCurrency,
    offers,
    activeOffers,
    getProductsKeys,
    getBodyProductsPrices,
    getProductIndexByKey,
    setProductPrices
  }
})
