import { type DatasetBasicInfos, type TableItem } from '@/utils'

const dummyItem: TableItem = {
  id: 'item',
  data: {
    'header-string': 'A simple string'
  }
}
const buildDummyItems = (n: number = 1000) => {
  return [...Array(n).keys()].map((i) => {
    const item = { ...dummyItem }
    item.id = `${item.id} #${i + 1}`
    item.data = {
      'header-string': `${item.id}`,
      'header-tag': 'mytag',
      'header-tag-allow-new': 'mynewtag',
      'header-tags': 'tag 1 | tag 2',
      'header-tags-allow-new': 'new tag 1',
      'header-long-text':
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ',
      'header-link': 'https://datami.multi.coop',
      'header-links': 'https://datami.multi.coop',
      'header-email': 'datami@multi.coop',
      'header-emails': 'datami@multi.coop',
      'header-image':
        'https://raw.githubusercontent.com/multi-coop/datami-website-content/main/images/logos/logo-DATAMI-rect-colors-03.png',
      'header-timelinetext': 'test',
      'header-integer': 6,
      'header-number': 12.4,
      'header-percent': 0.85,
      'header-geopoint-lon': 45.505331312,
      'header-geopoint-lat': -73.55249779,
      'header-boolean': 'true',
      'header-date': '2024-02-02'
    }
    return item
  })
}
const dummyItems: TableItem[] = buildDummyItems(200)

const dummyDataset: Dataset = {
  id: 'new-dataset',
  meta: {
    title: 'A new dataset',
    tags: ['Test']
  },
  headers: [
    {
      id: 'header-string',
      name: 'text',
      tableSchema: {
        type: TableHeaderType.string,
        title: 'Text',
        description: 'A simple text field'
      }
    },
    {
      id: 'header-tag',
      name: 'tag',
      tableSchema: {
        type: TableHeaderType.string,
        title: 'Tag',
        description: 'A tag selection field with a closed list for entries',
        constraints: {
          enum: ['tag-A', 'tag-B', 'tag-C', 'tag-D', 'tag-E', 'tag-F']
        }
      },
      customProps: {
        subtype: TableHeaderSubtype.tag,
        allowNew: false
      }
    },
    {
      id: 'header-tag-allow-new',
      name: 'tag & allow new',
      tableSchema: {
        type: TableHeaderType.string,
        title: 'Tag & allow new',
        description: 'A tag selection field allowing new entries',
        constraints: {
          enum: ['tag-A', 'tag-B', 'tag-C', 'tag-D', 'tag-E', 'tag-F']
        }
      },
      customProps: {
        subtype: TableHeaderSubtype.tag,
        allowNew: true
      }
    },
    {
      id: 'header-tags',
      name: 'tags',
      tableSchema: {
        type: TableHeaderType.string,
        title: 'Tags',
        description: 'A tags selection field, allowing multiple choices',
        constraints: {
          enum: ['tags-A', 'tags-B', 'tags-C', 'tags-D', 'tags-E', 'tags-F']
        }
      },
      customProps: {
        subtype: TableHeaderSubtype.tags,
        allowNew: false,
        tagSeparator: '|'
      }
    },
    {
      id: 'header-tags-allow-new',
      name: 'tags & allow new',
      tableSchema: {
        type: TableHeaderType.string,
        title: 'Tags & allow new',
        description: 'A tags selection field, allowing multiple choices and new entries',
        constraints: {
          enum: ['tags-A', 'tags-B', 'tags-C', 'tags-D', 'tags-E', 'tags-F']
        }
      },
      customProps: {
        subtype: TableHeaderSubtype.tags,
        allowNew: true,
        tagSeparator: '|'
      }
    },
    {
      id: 'header-long-text',
      name: 'long text',
      tableSchema: {
        type: TableHeaderType.string,
        description: 'A long text field',
        title: 'Long-text'
      },
      customProps: {
        subtype: TableHeaderSubtype.longtext
      }
    },
    {
      id: 'header-link',
      name: 'link',
      tableSchema: {
        type: TableHeaderType.string,
        description: 'A web link field',
        title: 'Link'
      },
      customProps: {
        subtype: TableHeaderSubtype.link
      }
    },
    {
      id: 'header-links',
      name: 'links',
      tableSchema: {
        type: TableHeaderType.string,
        description: 'A web links field, allowing multiple entries',
        title: 'Links'
      },
      customProps: {
        subtype: TableHeaderSubtype.links
      }
    },
    {
      id: 'header-email',
      name: 'email',
      tableSchema: {
        type: TableHeaderType.string,
        description: 'A email field',
        title: 'Email'
      },
      customProps: {
        subtype: TableHeaderSubtype.email
      }
    },
    {
      id: 'header-emails',
      name: 'emails',
      tableSchema: {
        type: TableHeaderType.string,
        description: 'A emails field, allowing multiple entries',
        title: 'Emails'
      },
      customProps: {
        subtype: TableHeaderSubtype.emails
      }
    },
    {
      id: 'header-image',
      name: 'image',
      tableSchema: {
        type: TableHeaderType.string,
        description: 'An image field',
        title: 'Image'
      },
      customProps: {
        subtype: TableHeaderSubtype.image
      }
    },
    {
      id: 'header-timelinetext',
      name: 'timelinetext',
      tableSchema: {
        type: TableHeaderType.string,
        description: 'A timeline field',
        title: 'Timeline text'
      },
      customProps: {
        subtype: TableHeaderSubtype.timelinetext
      }
    },
    {
      id: 'header-integer',
      name: 'integer',
      tableSchema: {
        type: TableHeaderType.integer,
        description: 'A integer field',
        title: 'Integer'
      }
    },
    {
      id: 'header-number',
      name: 'number',
      tableSchema: {
        type: TableHeaderType.number,
        description: 'A number field',
        title: 'Number'
      }
    },
    {
      id: 'header-percent',
      name: 'percent',
      tableSchema: {
        type: TableHeaderType.number,
        description: 'A percent field',
        title: 'Percent'
      },
      customProps: {
        subtype: TableHeaderSubtype.percent
      }
    },
    {
      id: 'header-geopoint-lon',
      name: 'geopoint lon',
      tableSchema: {
        type: TableHeaderType.number,
        description: 'A geopoint field',
        title: 'Geopoint lon'
      },
      customProps: {
        subtype: TableHeaderSubtype.geopoint
      }
    },
    {
      id: 'header-geopoint-lat',
      name: 'geopoint lat',
      tableSchema: {
        type: TableHeaderType.number,
        description: 'A geopoint field',
        title: 'Geopoint lat'
      },
      customProps: {
        subtype: TableHeaderSubtype.geopoint
      }
    },
    {
      id: 'header-boolean',
      name: 'boolean',
      tableSchema: {
        type: TableHeaderType.boolean,
        description: 'A boolean field',
        title: 'Boolean'
      }
    },
    {
      id: 'header-date',
      name: 'date',
      tableSchema: {
        type: TableHeaderType.date,
        description: 'A date field',
        title: 'Date'
      }
    }
  ],
  items: [
    {
      id: 'item-A',
      data: {
        'header-string': 'A simple string',
        'header-tag': 'mytag',
        'header-tag-allow-new': 'mynewtag',
        'header-tags': 'tag 1 | tag 2',
        'header-tags-allow-new': 'new tag 1',
        'header-long-text':
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ',
        'header-link': 'https://datami.multi.coop',
        'header-links': 'https://datami.multi.coop',
        'header-email': 'datami@multi.coop',
        'header-emails': 'datami@multi.coop',
        'header-image':
          'https://raw.githubusercontent.com/multi-coop/datami-website-content/main/images/logos/logo-DATAMI-rect-colors-03.png',
        'header-timelinetext': 'test',
        'header-integer': 6,
        'header-number': 12.4,
        'header-percent': 0.85,
        'header-geopoint-lon': 45.505331312,
        'header-geopoint-lat': -73.55249779,
        'header-boolean': 'true',
        'header-date': '2024-02-02'
      }
    },
    ...dummyItems
  ],
  gitInfos: []
}

// const dummyDatasetList: DatasetBasicInfos[] = [
//   { id: 'dataset-1', meta: { title: 'Dataset 1' } },
//   { id: 'dataset-2', meta: { title: 'Dataset 2' } },
//   { id: 'dataset-3', meta: { title: 'Dataset 3' } },
//   { id: 'dataset-4', meta: { title: 'Dataset 4' } }
// ]

export const useDatasetsStore = defineStore('datasets', () => {
  const pendingRequestDatasets = ref<boolean>()
  const statusRequestDatasets = ref<string>()
  const keyRequesthDatasets = ref<string>()

  const userDatasets = ref<DatasetBasicInfos[]>([])
  // const userDatasets = ref<DatasetBasicInfos[]>(dummyDatasetList)

  // const newDataset = ref<Dataset>()
  const newDataset = ref<Dataset>(dummyDataset)
  const currentDataset = ref<Dataset>()

  // ---------------------
  // DATASET
  // ---------------------

  // COMPUTED & GETTERS
  const getDatasetById = (datasetId: string) => {
    return userDatasets.value.find((d) => d.id === datasetId)
  }

  const getDatasetInfosById = (datasetId?: string, isNewDataset?: boolean) => {
    const excluded = ['headers', 'items']
    const dataset = isNewDataset ? newDataset.value : (userDatasets.value.find((d) => d.id === datasetId) as Dataset)

    // console.log(Object.keys(datasetId.value))
    const datasetInfos = Object.keys(dataset)
      .filter((k) => !excluded.includes(k))
      .reduce((obj, key) => {
        return {
          ...obj,
          [key]: dataset[key]
        }
      }, {})
    return datasetInfos
  }

  // SETTERS
  const setDataset = (dataset: Dataset, isNewDataset?: boolean) => {
    if (isNewDataset) {
      newDataset.value = dataset
    } else {
      currentDataset.value = dataset
    }
  }
  const updateDataset = ({ key, value }: { key: string; value: any }, isNewDataset?: boolean) => {
    // console.log('\n')
    // console.log('key : ', key)
    // console.log('value : ', value)
    if (isNewDataset) {
      newDataset.value = setProperty(newDataset.value, key, value) as Dataset
      // console.log('updateDataset > newDataset.value :', newDataset.value)
    } else {
      // const updated = currentDataset.value && (setProperty(toRaw(currentDataset.value), key, value) as Dataset)
      currentDataset.value = currentDataset.value && (setProperty(toRaw(currentDataset.value), key, value) as Dataset)
    }
  }
  const resetDataset = (isNewDataset?: boolean) => {
    if (isNewDataset) {
      newDataset.value = { ...dummyDataset }
    } else {
      currentDataset.value = undefined
    }
  }
  const removeDataset = (datasetId: string) => {
    userDatasets.value = userDatasets.value.filter((d) => d.id !== datasetId)
  }

  // HEADERS
  const updateDatasetHeaders = (headers: TableHeader[], isNewDataset?: boolean) => {
    if (isNewDataset) {
      newDataset.value.headers = headers
    }
    if (!isNewDataset && currentDataset.value) {
      currentDataset.value.headers = headers
    }
  }
  const addDatasetHeader = (header: TableHeader, isNewDataset?: boolean) => {
    if (isNewDataset) {
      newDataset.value.headers.push(header)
    } else {
      currentDataset.value?.headers.push(header)
    }
  }
  const updateDatasetHeader = (headerId: string, path: string, value: any, isNewDataset?: boolean) => {
    // console.log('updateDatasetHeader > headerId :', headerId)
    // console.log('updateDatasetHeader > path :', path)
    // console.log('updateDatasetHeader > datasetId :', datasetId)
    if (isNewDataset) {
      // console.log('updateDatasetHeader > isNewDataset :', isNewDataset)
      newDataset.value.headers.forEach((h, i) => {
        if (h.id === headerId) {
          // console.log('updateDatasetHeader > h :', h)
          const updatedHeader = setProperty(toRaw(h), path, value) as TableHeader
          // console.log('updateDatasetHeader > updatedHeader :', updatedHeader)
          newDataset.value.headers[i] = updatedHeader
        }
      })
    }
    if (!isNewDataset && currentDataset.value) {
      currentDataset.value.headers.forEach((h, i) => {
        if (h.id === headerId && currentDataset.value) {
          const updatedHeader = setProperty(toRaw(h), path, value) as TableHeader
          currentDataset.value.headers[i] = updatedHeader
        }
      })
    }
  }
  const removeDatasetHeader = (headerId: TableHeader['id'], isNewDataset?: boolean) => {
    if (isNewDataset) {
      newDataset.value.headers = newDataset.value.headers.filter((h) => h.id !== headerId)
      newDataset.value.items = newDataset.value.items.map((i) => {
        if (i.data) {
          delete i.data[headerId]
        }
        return i
      })
    }
    if (!isNewDataset && currentDataset.value) {
      currentDataset.value.headers = currentDataset.value.headers.filter((h) => h.id !== headerId)
      currentDataset.value.items = currentDataset.value.items.map((i) => {
        if (i.data) {
          delete i.data[headerId]
        }
        return i
      })
    }
  }

  // ITEMS
  const updateDatasetItems = (items: TableItem[], isNewDataset?: boolean) => {
    if (isNewDataset) {
      newDataset.value.items = items
    }
    if (!isNewDataset && currentDataset.value) {
      currentDataset.value.items = items
    }
  }
  const addDatasetItem = (item: TableItem, isNewDataset?: boolean) => {
    if (isNewDataset) {
      newDataset.value.items.push(item)
    } else {
      currentDataset.value?.items.push(item)
    }
  }
  const updateDatasetItem = (itemId: string, path: string, value: any, isNewDataset?: boolean) => {
    // console.log('\n')
    // console.log('itemId : ', itemId)
    // console.log('path : ', path)
    // console.log('value : ', value)
    if (isNewDataset) {
      newDataset.value.items.forEach((item, i) => {
        if (item.id === itemId) {
          // console.log('item : ', item)
          const updatedItem = setProperty(toRaw(item), path, value) as TableItem
          newDataset.value.items[i] = updatedItem
        }
      })
    }
    if (!isNewDataset && currentDataset.value) {
      currentDataset.value.items.forEach((item, i) => {
        if (item.id === itemId && currentDataset.value) {
          // console.log('item : ', item)
          const updatedItem = setProperty(toRaw(item), path, value) as TableItem
          currentDataset.value.items[i] = updatedItem
        }
      })
    }
  }
  const removeDatasetItem = (itemId: TableItem['id'], isNewDataset?: boolean) => {
    if (isNewDataset) {
      newDataset.value.items = newDataset.value.items.filter((i) => i.id !== itemId)
    }
    if (!isNewDataset && currentDataset.value) {
      currentDataset.value.items = currentDataset.value?.items.filter((i) => i.id !== itemId)
    }
  }

  // ---------------------
  // DATASETS
  // ---------------------

  // COMPUTED

  const userDatasetsLength = computed(() => {
    return userDatasets.value?.length
  })

  // SETTERS

  const setUserDatasets = (datasets: DatasetBasicInfos[]) => {
    userDatasets.value = datasets
  }

  return {
    pendingRequestDatasets,
    statusRequestDatasets,
    keyRequesthDatasets,
    userDatasets,
    newDataset,
    currentDataset,
    getDatasetById,
    getDatasetInfosById,
    setDataset,
    updateDataset,
    resetDataset,
    removeDataset,
    updateDatasetHeaders,
    addDatasetHeader,
    updateDatasetHeader,
    removeDatasetHeader,
    updateDatasetItems,
    addDatasetItem,
    updateDatasetItem,
    removeDatasetItem,
    userDatasetsLength,
    setUserDatasets
  }
})
