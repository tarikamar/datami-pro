import { type UserSubscription } from '@/utils'

export const useUserStore = defineStore('user', () => {
  const {
    status, //
    data
  } = useAuth()

  const userSubscription = ref<UserSubscription>()

  // setters
  const setUserSubscription = (subscription: UserSubscription) => {
    userSubscription.value = subscription
  }

  // computed
  const loggedIn = computed(() => status.value === 'authenticated')

  const userId = computed(() => {
    if (data.value) {
      return (data.value as SessionClient).userId
    }
  })

  const userInfos = computed(() => {
    if (data.value) {
      return (data.value as SessionClient).user
    }
  })

  const userEmail = computed(() => userInfos.value?.email)

  return {
    status,
    data,
    loggedIn,
    userId,
    userInfos,
    userEmail,
    userSubscription,
    setUserSubscription
  }
})
