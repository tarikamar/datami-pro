import { useTheme } from 'vuetify'

export const useNavigationStore = defineStore('navigation', () => {
  const theme = useTheme()

  const drawerLeft = ref<boolean>(false)
  const persistentDrawer = ref<boolean>(false)

  const currentTheme = computed(() => {
    return theme.global.name.value
  })
  const isDark = computed(() => {
    return currentTheme.value === 'dark'
  })
  function openLeftDrawer() {
    drawerLeft.value = true
  }
  function closeLeftDrawer() {
    if (!persistentDrawer.value) {
      drawerLeft.value = false
    }
  }
  function toogleLeftDrawer() {
    drawerLeft.value = !drawerLeft.value
    persistentDrawer.value = !persistentDrawer.value
  }

  return {
    currentTheme,
    isDark,
    drawerLeft,
    openLeftDrawer,
    closeLeftDrawer,
    toogleLeftDrawer
  }
})
