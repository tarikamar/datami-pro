export const useMessageStore = defineStore('message', () => {
  // const testError = {
  //   message: 'TEST ERROR 2',
  //   status: 404,
  //   statusCode: 404,
  //   statusMessage: 'TEST ERROR 2'
  // }

  // const storeError = ref<ErrorObj | undefined>(testError)
  const storeError = ref<ErrorObj | undefined>()

  const hasError = computed(() => {
    return !!storeError.value
  })

  const setError = (err: ErrorObj) => {
    console.log(err)
    storeError.value = err
  }
  const resetError = () => {
    storeError.value = undefined
  }

  return {
    storeError,
    hasError,
    setError,
    resetError
  }
})
