export default defineNuxtRouteMiddleware(async () => {
  const { getSession } = useAuth()
  const { getLastActiveSubscription } = useMyAuth()
  const userStore = useUserStore()

  const session = (await getSession()) as SessionClient
  const userId = session.userId
  // console.log('middleware > susbscription.global > userId : ', userId)

  const { data: subscription } = await getLastActiveSubscription({ userId })
  // console.log('middleware > susbscription.global > subscription.value : ', subscription.value)

  userStore.setUserSubscription(subscription.value as UserSubscription)

  // if (to.params.id === '1') {
  //   return abortNavigation()
  // }
  // // In a real app you would probably not redirect every route to `/`
  // // however it is important to check `to.path` before redirecting or you
  // // might get an infinite redirect loop
  // if (to.path !== '/') {
  //   return navigateTo('/')
  // }
})
