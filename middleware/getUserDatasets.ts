import { type DatasetBasicInfos } from '@/utils'

const { getSession } = useAuth()
const { getUserDatasets } = useDatasets()
const datasetsStore = useDatasetsStore()
const messages = useMessageStore()

export default defineNuxtRouteMiddleware(async () => {
  const session = (await getSession()) as SessionClient
  const userId = session.userId
  // console.log('middleware > getUserDatasets > userId : ', userId)

  const {
    data: userDatasets, //
    pending,
    status,
    error,
    // refresh,
    requestKey
  } = await getUserDatasets({ userId })
  console.log('middleware > getUserDatasets > userDatasets.value : ', userDatasets.value)
  console.log('middleware > getUserDatasets > pending.value : ', pending.value)

  datasetsStore.pendingRequestDatasets = pending.value
  datasetsStore.statusRequestDatasets = status.value
  datasetsStore.keyRequesthDatasets = requestKey.value

  if (error !== null) {
    messages.setError(error.value as ErrorObj)
  }

  console.log('middleware > getUserDatasets > pending.value : ', pending.value)
  datasetsStore.setUserDatasets(userDatasets.value as DatasetBasicInfos[])
})
