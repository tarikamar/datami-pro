import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

import { createVuetify, type ThemeDefinition } from 'vuetify'
// import DateFnsAdapter from '@date-io/date-fns'
// import fr from 'date-fns/locale/fr'
// import en from 'date-fns/locale/en-US'
import { fr, en } from 'vuetify/locale'

const datamiColors = {
  datamiRed: '#d73027ff',
  datamiOrange: '#fc8d59ff',
  datamiYellow: '#fee090ff',
  datamiBLuePale: '#e0f3f8ff',
  // datamiBLuePaleTransparent: 'rgba(224, 243, 248, 0.5)',
  datamiBlueLight: '#91bfdbff',
  datamiBlueDark: '#4575b4ff'
}

const customLightTheme: ThemeDefinition = {
  dark: false,
  colors: {
    surface: '#FFFFFF',
    // background: '#d5e0de',
    background: datamiColors.datamiBLuePale,
    // backgroundTransparent: datamiColors.datamiBLuePaleTransparent,
    // background: '#FFFFFF',
    // surface: '#d5e0de',
    primary: datamiColors.datamiOrange,
    // 'primary-darken-1': '#3700B3',
    // secondary: '#03DAC6',
    // 'secondary-darken-1': '#018786',
    error: '#B00020',
    // info: '#2196F3',
    success: '#4CAF50',
    warning: '#FB8C00',
    // grey: 'grey',
    ...datamiColors
  }
}
const customDarkTheme: ThemeDefinition = {
  dark: true,
  colors: {
    // background: '#071311',
    backgroundTransparent: 'rgba(255,255, 255, .5)',
    // surface: '#0f2723',
    primary: datamiColors.datamiOrange,
    // 'primary-darken-1': '#3700B3',
    // secondary: '#03DAC6',
    // 'secondary-darken-1': '#018786',
    error: '#B00020',
    // info: '#2196F3',
    success: '#4CAF50',
    warning: '#FB8C00',
    // grey: 'whitesmoke',
    ...datamiColors
  }
}

export default defineNuxtPlugin((app) => {
  const vuetify = createVuetify({
    ssr: true,
    theme: {
      defaultTheme: 'light',
      themes: {
        light: customLightTheme,
        dark: customDarkTheme
      }
    },
    locale: { locale: 'fr', fallback: 'en', messages: { en, fr } }
    // date: {
    //   adapter: DateFnsAdapter,
    //   locale: {
    //     fr,
    //     en
    //   }
    // }
  })
  app.vueApp.use(vuetify)
})
