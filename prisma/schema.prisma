// This is your Prisma schema file,
// learn more about it in the docs: https://pris.ly/d/prisma-schema

generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

// -------------------------------
// USER RELATED
// -------------------------------

model User {
  id             String    @id @default(cuid())
  email          String    @unique
  emailVerified  DateTime?
  email_verified Boolean   @default(false)

  // DEPRECATED FOR NOW - PASSWORDLESS APPROACH
  password String? @db.VarChar(255)

  // PERSONAL INFOS
  name       String?
  first_name String? @db.VarChar(255)
  last_name  String? @db.VarChar(255)
  avatar     String? @db.VarChar(1000)
  image      String?

  // BILLING - STRIPE
  stripeCustomerId String?

  // RELATIONS
  account         Account[]              @relation("UserAccounts")
  sessions        Session[]              @relation("UserSessions")
  profile         Profile?               @relation("UserProfile")
  subscriptions   Subscription[]         @relation("UserSubscriptions")
  myOrganisations Organisation[]         @relation("UserMyOrganisations")
  organisations   OrganisationsOnUsers[] @relation("UserOrganisations")

  // DATAMI
  myWidgets  Widget[]          @relation("UserMyWidgets")
  widgets    WidgetsOnUsers[]  @relation("UserWidgets")
  myDatasets Dataset[]         @relation("UserMyDatasets")
  datasets   DatasetsOnUsers[] @relation("UserDatasets")

  // PERMISSIONS
  role        Role    @default(GENERAL)
  permissions String? @db.VarChar(4000)
  is_active   Boolean @default(true)

  // META - ACTIVITY
  last_login DateTime? @db.Timestamptz(0)
  created_at DateTime  @default(now()) @db.Timestamptz(0)
  deleted_at DateTime? @db.Timestamptz(0)
  updated_at DateTime? @updatedAt
}

enum Role {
  SUPER_ADMIN
  ADMIN
  GENERAL
}

// -------------------------------
// ACCOUNT RELATED
// -------------------------------

model Account {
  // DEFAULT NEXT-AUTH 
  id                String  @id @default(cuid())
  userId            String
  type              String
  provider          String
  providerAccountId String
  refresh_token     String? @db.Text
  access_token      String? @db.Text
  expires_at        Int?
  token_type        String?
  scope             String?
  id_token          String? @db.Text
  session_state     String?

  // PROVIDER RELATED
  ext_expires_at           Int?
  refresh_token_expires_in Int? // cf : https://next-auth.js.org/providers/github
  created_at               Int? // cf : https://next-auth.js.org/providers/gitlab

  // GITHUB - TEMP
  githubLogin String?
  githubUrl   String?
  githubRepos String?

  // GITLAB - TEMP
  gitlabLogin String?
  gitlabUrl   String?
  gitlabRepos String?

  // RELATIONS
  user User @relation("UserAccounts", fields: [userId], references: [id], onDelete: Cascade)

  @@unique([provider, providerAccountId])
}

// -------------------------------
// PROFILE RELATED
// -------------------------------

model Profile {
  // DEFAULT
  id     String  @id @default(cuid())
  userId String  @unique
  email  String  @unique
  bio    String? @db.Text
  locale String?

  // BILLING - STRIPE
  subscribed    Boolean   @default(false)
  subscribedAt  DateTime?
  planType      Plan      @default(FREE)
  planCreatedAt DateTime? @default(now())
  planUpdatedAt DateTime?
  planExpiresAt DateTime?

  // META
  createdAt DateTime? @default(now())

  // GITHUB
  githubLogin String?
  githubUrl   String?
  githubRepos String?

  // GITLAB
  gitlabLogin String?
  gitlabUrl   String?
  gitlabRepos String?

  // RELATIONS
  subscriptions Subscription[]            @relation("ProfileSubscriptions")
  organisations OrganisationsOnProfiles[] @relation("ProfileOrganisations")
  widgets       WidgetsOnProfiles[]       @relation("ProfileWidgets")
  datasets      DatasetsOnProfiles[]      @relation("ProfileDatasets")
  user          User                      @relation("UserProfile", fields: [userId], references: [id], onDelete: Cascade)
}

enum Plan {
  FREE
  BASIC
  PERSONAL
  INTERMEDIATE
  SMALL_ORGANISATION
  PROFESSIONAL
}

// -------------------------------
// SECURITY RELATED
// -------------------------------

model Session {
  // DEFAULT NEXT-AUTH 
  id           String    @id @default(cuid())
  sessionToken String    @unique
  userId       String
  expires      DateTime?

  // META 
  createdAt DateTime @default(now())

  // RELATIONS
  user User @relation("UserSessions", fields: [userId], references: [id], onDelete: Cascade)
}

// DEPRECATED ? 
model VerificationToken {
  // DEFAULT NEXT-AUTH 
  identifier String
  token      String   @unique
  expires    DateTime

  // META 
  createdAt DateTime  @default(now())
  updatedAt DateTime? @updatedAt

  @@unique([identifier, token])
}

// -------------------------------
// ORGANISATION RELATED
// -------------------------------

model Organisation {
  // GLOBAL
  id           String  @id @default(cuid())
  url          String?
  name         String?
  zipCode      String?
  taxNumber    String?
  country      String?
  contactEmail String?

  // BILLING
  planType Plan @default(FREE)

  // META
  createdByUserId String    @unique
  createdAt       DateTime? @default(now())

  // RELATIONS
  author       User                      @relation("UserMyOrganisations", fields: [createdByUserId], references: [id])
  users        OrganisationsOnUsers[]    @relation("OrganisationUsers")
  profiles     OrganisationsOnProfiles[] @relation("OrganisationProfiles")
  subscription Subscription[]            @relation("OrganisationSubscriptions")
}

model OrganisationsOnUsers {
  organisation   Organisation @relation("OrganisationUsers", fields: [organisationId], references: [id])
  organisationId String
  user           User         @relation("UserOrganisations", fields: [userId], references: [id])
  userId         String

  @@id([organisationId, userId])
}

model OrganisationsOnProfiles {
  organisation   Organisation @relation("OrganisationProfiles", fields: [organisationId], references: [id])
  organisationId String
  profile        Profile      @relation("ProfileOrganisations", fields: [profileId], references: [id])
  profileId      String

  @@id([organisationId, profileId])
}

// -------------------------------
// SUBSCRIPTION RELATED
// -------------------------------

enum SubscriptionCategory {
  FREE
  PERSONAL
  PROFESSIONAL
  PREMIUM
}

model Subscription {
  id             String  @id @default(cuid())
  userId         String
  profileId      String
  organisationId String?

  // STRIPE INFOS
  stripeId              String  @unique // aka subscription id from Stripe
  stripeStatus          String?
  stripeProductKey      String?
  stripeProductNickName String?
  stripePriceId         String?
  quantity              Int?
  amount                Int?
  currency              String?
  trialEndsAt           Int?
  endsAt                Int?
  startDate             Int?
  lastEventDate         Int?

  // AUTH
  subscriptionCategory SubscriptionCategory @default(PERSONAL)
  numberUsers          Int?
  numberDatasets       Int?
  numberWidgets        Int?

  // RELATIONS
  user         User          @relation("UserSubscriptions", fields: [userId], references: [id])
  profile      Profile?      @relation("ProfileSubscriptions", fields: [profileId], references: [id])
  organisation Organisation? @relation("OrganisationSubscriptions", fields: [organisationId], references: [id])
}

// -------------------------------
// WIDGET RELATED
// -------------------------------

model Widget {
  id    String  @id @default(cuid())
  isSet Boolean @default(false)

  // SETTINGS
  title   Json?
  options Json?

  // META
  createdByUserId String    @unique
  createdAt       DateTime? @default(now())

  // RELATIONS
  author   User                @relation("UserMyWidgets", fields: [createdByUserId], references: [id])
  users    WidgetsOnUsers[]    @relation("WidgetUsers")
  datasets WidgetsOnDatasets[] @relation("WidgetDatasets")
  profiles WidgetsOnProfiles[] @relation("WidgetProfiles")
}

model WidgetsOnUsers {
  widget   Widget @relation("WidgetUsers", fields: [widgetId], references: [id])
  widgetId String // relation scalar field (used in the `@relation` attribute above)
  user     User   @relation("UserWidgets", fields: [userId], references: [id])
  userId   String // relation scalar field (used in the `@relation` attribute above)

  @@id([widgetId, userId])
}

model WidgetsOnDatasets {
  widget    Widget  @relation("WidgetDatasets", fields: [widgetId], references: [id])
  widgetId  String // relation scalar field (used in the `@relation` attribute above)
  dataset   Dataset @relation("DatasetWidgets", fields: [datasetId], references: [id])
  datasetId String // relation scalar field (used in the `@relation` attribute above)

  @@id([widgetId, datasetId])
}

model WidgetsOnProfiles {
  widget    Widget  @relation("WidgetProfiles", fields: [widgetId], references: [id])
  widgetId  String // relation scalar field (used in the `@relation` attribute above)
  profile   Profile @relation("ProfileWidgets", fields: [profileId], references: [id])
  profileId String // relation scalar field (used in the `@relation` attribute above)

  @@id([widgetId, profileId])
}

// -------------------------------
// DATASET RELATED
// -------------------------------

model Dataset {
  // GLOBAL
  id       String @id @default(cuid())
  meta     Json?
  headers  Json?
  items    Json?
  gitInfos Json?

  // tags        String[]
  // onlyPreview Boolean  @default(false)

  // DATA MODEL FILES
  schemaFile      String?
  customPropsFile String?

  // VALIDATORS
  // provider String?
  // openData Boolean @default(false)

  // HTML - WIDGET
  widgetSettings        Json?
  globalFiltersSettings Json?

  // TABLE
  tableSettings Json?

  // CARDS
  cardsMiniSettings   Json?
  cardsDetailSettings Json?

  // MAP
  mapSettings Json?

  // DATAVIZ
  datavizSettings Json?

  // INTERNAL META
  isSet           Boolean   @default(false)
  createdByUserId String
  createdAt       DateTime? @default(now())

  // RELATIONS
  author   User                 @relation("UserMyDatasets", fields: [createdByUserId], references: [id])
  users    DatasetsOnUsers[]    @relation("DatasetUsers")
  profiles DatasetsOnProfiles[] @relation("DatasetProfiles")
  widgets  WidgetsOnDatasets[]  @relation("DatasetWidgets")
}

model DatasetsOnProfiles {
  profile   Profile @relation("ProfileDatasets", fields: [profileId], references: [id])
  profileId String // relation scalar field (used in the `@relation` attribute above)
  dataset   Dataset @relation("DatasetProfiles", fields: [datasetId], references: [id])
  datasetId String // relation scalar field (used in the `@relation` attribute above)

  @@id([profileId, datasetId])
}

model DatasetsOnUsers {
  user      User    @relation("UserDatasets", fields: [userId], references: [id])
  userId    String
  dataset   Dataset @relation("DatasetUsers", fields: [datasetId], references: [id])
  datasetId String

  @@id([userId, datasetId])
}
