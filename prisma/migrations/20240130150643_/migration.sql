-- CreateEnum
CREATE TYPE "Role" AS ENUM ('SUPER_ADMIN', 'ADMIN', 'GENERAL');

-- CreateEnum
CREATE TYPE "Plan" AS ENUM ('FREE', 'BASIC', 'PERSONAL', 'INTERMEDIATE', 'SMALL_ORGANISATION', 'PROFESSIONAL');

-- CreateEnum
CREATE TYPE "SubscriptionCategory" AS ENUM ('PERSONAL', 'PROFESSIONAL');

-- CreateTable
CREATE TABLE "User" (
    "id" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "emailVerified" TIMESTAMP(3),
    "email_verified" BOOLEAN NOT NULL DEFAULT false,
    "password" VARCHAR(255),
    "name" TEXT,
    "first_name" VARCHAR(255),
    "last_name" VARCHAR(255),
    "avatar" VARCHAR(1000),
    "image" TEXT,
    "stripeCustomerId" TEXT,
    "role" "Role" NOT NULL DEFAULT 'GENERAL',
    "permissions" VARCHAR(4000),
    "is_active" BOOLEAN NOT NULL DEFAULT true,
    "last_login" TIMESTAMPTZ(0),
    "created_at" TIMESTAMPTZ(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deleted_at" TIMESTAMPTZ(0),
    "updated_at" TIMESTAMP(3),

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Account" (
    "id" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "provider" TEXT NOT NULL,
    "providerAccountId" TEXT NOT NULL,
    "refresh_token" TEXT,
    "access_token" TEXT,
    "expires_at" INTEGER,
    "token_type" TEXT,
    "scope" TEXT,
    "id_token" TEXT,
    "session_state" TEXT,
    "ext_expires_at" INTEGER,
    "refresh_token_expires_in" INTEGER,
    "created_at" INTEGER,
    "githubLogin" TEXT,
    "githubUrl" TEXT,
    "githubRepos" TEXT,
    "gitlabLogin" TEXT,
    "gitlabUrl" TEXT,
    "gitlabRepos" TEXT,

    CONSTRAINT "Account_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Profile" (
    "id" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "bio" TEXT,
    "locale" TEXT,
    "subscribed" BOOLEAN NOT NULL DEFAULT false,
    "subscribedAt" TIMESTAMP(3),
    "planType" "Plan" NOT NULL DEFAULT 'FREE',
    "planCreatedAt" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "planUpdatedAt" TIMESTAMP(3),
    "planExpiresAt" TIMESTAMP(3),
    "createdAt" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "githubLogin" TEXT,
    "githubUrl" TEXT,
    "githubRepos" TEXT,
    "gitlabLogin" TEXT,
    "gitlabUrl" TEXT,
    "gitlabRepos" TEXT,

    CONSTRAINT "Profile_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Session" (
    "id" TEXT NOT NULL,
    "sessionToken" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "expires" TIMESTAMP(3),
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "Session_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "VerificationToken" (
    "identifier" TEXT NOT NULL,
    "token" TEXT NOT NULL,
    "expires" TIMESTAMP(3) NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3)
);

-- CreateTable
CREATE TABLE "Organisation" (
    "id" TEXT NOT NULL,
    "url" TEXT,
    "name" TEXT,
    "zipCode" TEXT,
    "taxNumber" TEXT,
    "country" TEXT,
    "contactEmail" TEXT,
    "planType" "Plan" NOT NULL DEFAULT 'FREE',
    "createdByUserId" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "Organisation_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "OrganisationsOnUsers" (
    "organisationId" TEXT NOT NULL,
    "userId" TEXT NOT NULL,

    CONSTRAINT "OrganisationsOnUsers_pkey" PRIMARY KEY ("organisationId","userId")
);

-- CreateTable
CREATE TABLE "OrganisationsOnProfiles" (
    "organisationId" TEXT NOT NULL,
    "profileId" TEXT NOT NULL,

    CONSTRAINT "OrganisationsOnProfiles_pkey" PRIMARY KEY ("organisationId","profileId")
);

-- CreateTable
CREATE TABLE "Subscription" (
    "id" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "profileId" TEXT NOT NULL,
    "organisationId" TEXT,
    "stripeId" TEXT NOT NULL,
    "stripeStatus" TEXT,
    "stripePriceId" TEXT,
    "quantity" INTEGER,
    "amount" INTEGER,
    "currency" TEXT,
    "trialEndsAt" INTEGER,
    "endsAt" INTEGER,
    "startDate" INTEGER,
    "lastEventDate" INTEGER,
    "subscriptionCategory" "SubscriptionCategory" NOT NULL DEFAULT 'PERSONAL',

    CONSTRAINT "Subscription_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Widget" (
    "id" TEXT NOT NULL,
    "isSet" BOOLEAN NOT NULL DEFAULT false,
    "title" JSONB,
    "options" JSONB,
    "createdByUserId" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "Widget_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "WidgetsOnUsers" (
    "widgetId" TEXT NOT NULL,
    "userId" TEXT NOT NULL,

    CONSTRAINT "WidgetsOnUsers_pkey" PRIMARY KEY ("widgetId","userId")
);

-- CreateTable
CREATE TABLE "WidgetsOnDatasets" (
    "widgetId" TEXT NOT NULL,
    "datasetId" TEXT NOT NULL,

    CONSTRAINT "WidgetsOnDatasets_pkey" PRIMARY KEY ("widgetId","datasetId")
);

-- CreateTable
CREATE TABLE "WidgetsOnProfiles" (
    "widgetId" TEXT NOT NULL,
    "profileId" TEXT NOT NULL,

    CONSTRAINT "WidgetsOnProfiles_pkey" PRIMARY KEY ("widgetId","profileId")
);

-- CreateTable
CREATE TABLE "Dataset" (
    "id" TEXT NOT NULL,
    "url" TEXT NOT NULL,
    "provider" TEXT,
    "openData" BOOLEAN NOT NULL DEFAULT false,
    "isSet" BOOLEAN NOT NULL DEFAULT false,
    "tableSchema" JSONB,
    "customProps" JSONB,
    "widgetSettings" JSONB,
    "globalFiltersSettings" JSONB,
    "tableSettings" JSONB,
    "cardsMiniSettings" JSONB,
    "cardsDetailSettings" JSONB,
    "mapSettings" JSONB,
    "datavizSettings" JSONB,
    "createdByUserId" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "Dataset_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "DatasetsOnProfiles" (
    "profileId" TEXT NOT NULL,
    "datasetId" TEXT NOT NULL,

    CONSTRAINT "DatasetsOnProfiles_pkey" PRIMARY KEY ("profileId","datasetId")
);

-- CreateTable
CREATE TABLE "DatasetsOnUsers" (
    "userId" TEXT NOT NULL,
    "datasetId" TEXT NOT NULL,

    CONSTRAINT "DatasetsOnUsers_pkey" PRIMARY KEY ("userId","datasetId")
);

-- CreateIndex
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");

-- CreateIndex
CREATE UNIQUE INDEX "Account_provider_providerAccountId_key" ON "Account"("provider", "providerAccountId");

-- CreateIndex
CREATE UNIQUE INDEX "Profile_userId_key" ON "Profile"("userId");

-- CreateIndex
CREATE UNIQUE INDEX "Profile_email_key" ON "Profile"("email");

-- CreateIndex
CREATE UNIQUE INDEX "Session_sessionToken_key" ON "Session"("sessionToken");

-- CreateIndex
CREATE UNIQUE INDEX "VerificationToken_token_key" ON "VerificationToken"("token");

-- CreateIndex
CREATE UNIQUE INDEX "VerificationToken_identifier_token_key" ON "VerificationToken"("identifier", "token");

-- CreateIndex
CREATE UNIQUE INDEX "Organisation_createdByUserId_key" ON "Organisation"("createdByUserId");

-- CreateIndex
CREATE UNIQUE INDEX "Subscription_stripeId_key" ON "Subscription"("stripeId");

-- CreateIndex
CREATE UNIQUE INDEX "Widget_createdByUserId_key" ON "Widget"("createdByUserId");

-- CreateIndex
CREATE UNIQUE INDEX "Dataset_createdByUserId_key" ON "Dataset"("createdByUserId");

-- AddForeignKey
ALTER TABLE "Account" ADD CONSTRAINT "Account_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Profile" ADD CONSTRAINT "Profile_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Session" ADD CONSTRAINT "Session_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Organisation" ADD CONSTRAINT "Organisation_createdByUserId_fkey" FOREIGN KEY ("createdByUserId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrganisationsOnUsers" ADD CONSTRAINT "OrganisationsOnUsers_organisationId_fkey" FOREIGN KEY ("organisationId") REFERENCES "Organisation"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrganisationsOnUsers" ADD CONSTRAINT "OrganisationsOnUsers_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrganisationsOnProfiles" ADD CONSTRAINT "OrganisationsOnProfiles_organisationId_fkey" FOREIGN KEY ("organisationId") REFERENCES "Organisation"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrganisationsOnProfiles" ADD CONSTRAINT "OrganisationsOnProfiles_profileId_fkey" FOREIGN KEY ("profileId") REFERENCES "Profile"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Subscription" ADD CONSTRAINT "Subscription_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Subscription" ADD CONSTRAINT "Subscription_profileId_fkey" FOREIGN KEY ("profileId") REFERENCES "Profile"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Subscription" ADD CONSTRAINT "Subscription_organisationId_fkey" FOREIGN KEY ("organisationId") REFERENCES "Organisation"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Widget" ADD CONSTRAINT "Widget_createdByUserId_fkey" FOREIGN KEY ("createdByUserId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WidgetsOnUsers" ADD CONSTRAINT "WidgetsOnUsers_widgetId_fkey" FOREIGN KEY ("widgetId") REFERENCES "Widget"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WidgetsOnUsers" ADD CONSTRAINT "WidgetsOnUsers_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WidgetsOnDatasets" ADD CONSTRAINT "WidgetsOnDatasets_widgetId_fkey" FOREIGN KEY ("widgetId") REFERENCES "Widget"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WidgetsOnDatasets" ADD CONSTRAINT "WidgetsOnDatasets_datasetId_fkey" FOREIGN KEY ("datasetId") REFERENCES "Dataset"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WidgetsOnProfiles" ADD CONSTRAINT "WidgetsOnProfiles_widgetId_fkey" FOREIGN KEY ("widgetId") REFERENCES "Widget"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WidgetsOnProfiles" ADD CONSTRAINT "WidgetsOnProfiles_profileId_fkey" FOREIGN KEY ("profileId") REFERENCES "Profile"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Dataset" ADD CONSTRAINT "Dataset_createdByUserId_fkey" FOREIGN KEY ("createdByUserId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "DatasetsOnProfiles" ADD CONSTRAINT "DatasetsOnProfiles_profileId_fkey" FOREIGN KEY ("profileId") REFERENCES "Profile"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "DatasetsOnProfiles" ADD CONSTRAINT "DatasetsOnProfiles_datasetId_fkey" FOREIGN KEY ("datasetId") REFERENCES "Dataset"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "DatasetsOnUsers" ADD CONSTRAINT "DatasetsOnUsers_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "DatasetsOnUsers" ADD CONSTRAINT "DatasetsOnUsers_datasetId_fkey" FOREIGN KEY ("datasetId") REFERENCES "Dataset"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
