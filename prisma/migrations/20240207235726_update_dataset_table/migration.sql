/*
  Warnings:

  - You are about to drop the column `customProps` on the `Dataset` table. All the data in the column will be lost.
  - You are about to drop the column `openData` on the `Dataset` table. All the data in the column will be lost.
  - You are about to drop the column `provider` on the `Dataset` table. All the data in the column will be lost.
  - You are about to drop the column `tableSchema` on the `Dataset` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Dataset" DROP COLUMN "customProps",
DROP COLUMN "openData",
DROP COLUMN "provider",
DROP COLUMN "tableSchema";
