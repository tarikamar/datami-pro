/*
  Warnings:

  - You are about to drop the column `url` on the `Dataset` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Dataset" DROP COLUMN "url",
ADD COLUMN     "gitInfos" JSONB,
ADD COLUMN     "headers" JSONB,
ADD COLUMN     "items" JSONB,
ADD COLUMN     "meta" JSONB;
