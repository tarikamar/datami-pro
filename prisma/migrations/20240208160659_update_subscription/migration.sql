-- AlterTable
ALTER TABLE "Subscription" ADD COLUMN     "numberDatasets" INTEGER,
ADD COLUMN     "numberUsers" INTEGER,
ADD COLUMN     "numberWidgets" INTEGER;
