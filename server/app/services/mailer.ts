import { createTransport } from 'nodemailer'

const config = useRuntimeConfig()

export const nodemailerConfig = {
  server: {
    host: config.EMAIL_SERVER_HOST,
    port: config.EMAIL_SERVER_PORT,
    secure: true,
    auth: {
      user: config.EMAIL_SERVER_USER,
      pass: config.EMAIL_SERVER_PASSWORD
    }
  }
}

// export const nodemailerTransport = createTransport(nodemailerConfig)

// --------------------------
// EMAILING
// --------------------------

/** Email Text body (fallback for email clients that don't render HTML, e.g. feature phones) */
export const text = ({ url, host }: { url: string; host: string }) => {
  return `Sign in to ${host}\n${url}\n\n`
}

export const customVerificationEmail = async (to: string, url: string, server: object, from: string) => {
  console.log()
  console.log('sendVerificationRequest > to : ', to)
  console.log('sendVerificationRequest > url : ', url)
  console.log('sendVerificationRequest > server : ', server)
  console.log('sendVerificationRequest > from : ', from)
  const { host } = new URL(url)
  const transport = createTransport(server)
  const result = await transport.sendMail({
    to,
    from,
    subject: `Sign in to ${host}`,
    text: text({ url, host })
    // html: html({ url, host, theme }),
  })
  const failed = result.rejected.concat(result.pending).filter(Boolean)
  if (failed.length) {
    throw new Error(`Email (${failed.join(', ')}) could not be sent`)
  }
}
