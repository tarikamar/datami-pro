import Stripe from 'stripe'
import {
  type SubPostRes, //
  type UserDataBasic,
  type SubscriptionFromStripe,
  SubscriptionCategory,
  type Subscription,
  type Offer,
  OfferFeatureCode
} from '~/utils'
import {
  getSubscriptionById, //
  getUserByStripeCustomerId,
  createOrUpdateSubscription
  // getProfileByUserId
} from '~/server/database'
import { datamiProducts } from '@/data/products'

const config = useRuntimeConfig()
const stripe = new Stripe(config.STRIPE_SECRET_KEY)

// --------------------------
// SUBSCRIPTIONS
// --------------------------

/**
 * @desc Get a subscription url
 * @param lookupKey Price key
 * @param user User data
 */
export async function getSubscribeUrl(lookupKey: string, user: UserDataBasic): Promise<SubPostRes> {
  // console.log()
  // console.log('stripeService > getSubscribeUrl > lookupKey :', lookupKey)
  // console.log('stripeService > getSubscribeUrl > user :', user)

  const customerEmail = user.email
  // console.log('stripeService > getSubscribeUrl > customerEmail :', customerEmail)

  const price = await stripe.prices.retrieve(lookupKey)
  console.log('stripeService > getSubscribeUrl > price :', price)

  let shouldUpdateUser = false

  if (!user.stripeCustomerId) {
    shouldUpdateUser = true
    const customer = await stripe.customers.create({ email: customerEmail })
    user.stripeCustomerId = customer.id
    console.log('stripeService > getSubscribeUrl > customer :', customer)
  }

  try {
    const stripeSession = await stripe.checkout.sessions.create({
      billing_address_collection: 'auto',
      line_items: [
        {
          price: price.id,
          quantity: 1
        }
      ],
      mode: 'subscription',

      // NOT EMBEDDED
      // success_url: `${config.public.APP_DOMAIN}/subscribe/success?session_id={CHECKOUT_SESSION_ID}`,
      // cancel_url: `${config.public.APP_DOMAIN}/subscribe/cancel`,

      // EMBEDDED
      ui_mode: 'embedded',
      // return_url: `${config.public.APP_DOMAIN}/subscribe/success?session_id={CHECKOUT_SESSION_ID}`,
      redirect_on_completion: 'never',
      customer: user.stripeCustomerId
    })
    console.log('stripeService > getSubscribeUrl > stripeSession :', stripeSession)
    console.log('stripeService > getSubscribeUrl > test ')

    return {
      user,
      shouldUpdateUser,
      // url: stripeSession.url as string,
      // return_url: stripeSession.return_url as string,
      clientSecret: stripeSession.client_secret as string
    }
  } catch (error) {
    console.log('stripeService > getSubscribeUrl > error :', error)
    return {
      user,
      shouldUpdateUser,
      error
    }
  }
}

/**
 * @desc Handle a subscription event and update the table
 * @param subscription Subscription data
 * @param lastEventDate Last event date as number
 */
export async function handleSubscriptionChange(
  subscription: SubscriptionFromStripe,
  // subCategory: SubscriptionCategory,
  lastEventDate: number
): Promise<boolean> {
  const localSubscription = await getSubscriptionById(subscription.id)
  console.log('stripeService > handleSubscriptionChange > localSubscription :', localSubscription)
  // console.log('stripeService > handleSubscriptionChange > subscription.items.data :', subscription.items.data)

  if (localSubscription?.lastEventDate && localSubscription?.lastEventDate > lastEventDate) {
    return true
  }

  const stripeProductNickName = subscription.plan?.nickname // product?.nickName
  const stripeCustomerId = subscription.customer as string
  const user = await getUserByStripeCustomerId(stripeCustomerId)

  // TO DO - Get correct subscription category
  const stripeProductKey = subscription.plan?.product
  console.log('stripeService > handleSubscriptionChange > stripeProductKey :', stripeProductKey)
  const product = datamiProducts.find((p: Offer) => p.stripeProductKey === stripeProductKey)
  const subCategory = product?.subscriptionCategory || SubscriptionCategory.FREE
  const featUsers = product?.features?.find((feat) => feat.featCode === OfferFeatureCode.N_USERS)
  const featDatasets = product?.features?.find((feat) => feat.featCode === OfferFeatureCode.N_DATASETS)
  const featWidgets = product?.features?.find((feat) => feat.featCode === OfferFeatureCode.N_WIDGETS)

  if (!user) {
    throw createError({
      statusCode: 500,
      statusMessage: 'Something bad happened on the server while handling subscription'
    })
  } else {
    const data = {
      userId: user.id,
      profileId: user.profile?.id,
      // organisationId: // TO DO,
      name: subscription.id,
      stripeId: subscription.id,
      stripeStatus: subscription.status,
      stripeProductKey: stripeProductKey,
      stripeProductNickName: stripeProductNickName,
      stripePriceId: subscription.items.data[0].price.id,
      quantity: subscription.quantity,
      amount: subscription.items.data[0].price.unit_amount,
      currency: subscription.items.data[0].price.currency,
      trialEndsAt: subscription.trial_end,
      endsAt: subscription.ended_at,
      startDate: subscription.start_date,
      lastEventDate,
      subscriptionCategory: subCategory,
      numberUsers: featUsers?.amount,
      numberDatasets: featDatasets?.amount,
      numberWidgets: featWidgets?.amount
    } as unknown as Subscription
    console.log('stripeService > handleSubscriptionChange > data :', data)

    await createOrUpdateSubscription(data)

    return true
  }
}

// --------------------------
// PRODUCTS AND PRICES
// --------------------------

export async function getProductById(productKey: string) {
  return await stripe.products.retrieve(productKey)
}

export async function getAllProductsByIds(productKeys: string[]) {
  return await stripe.products.list({ ids: productKeys })
}

export async function getProductPrices(productKey: string) {
  const prices = await stripe.prices.list({ product: productKey })
  return prices.data
}
