import { H3Event } from 'h3'
import { getMappedError } from '~/server/app/errors/errorMapper'

export default function sendDefaultErrorResponse(
  event: H3Event, //
  errorType: string,
  statusCode: number,
  error: any
) {
  console.log('error.message :', error.message)
  const parsedErrors = getMappedError(errorType, error)
  // console.log('parsedErrors :', parsedErrors)
  // return sendError(event, createError({ statusCode, statusMessage: error.message, data: parsedErrors }))
  return createError({ statusCode, statusMessage: error.message, data: parsedErrors })
}
