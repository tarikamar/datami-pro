import { getServerSession } from '#auth'
import {
  type SessionClient, //
  type UserSubscription,
  // SubscriptionCategoryForPrisma,
  SubscriptionCategory
} from '@/utils'
import { getLastActiveSubscriptionByUserId } from '@/server/database'

const publicPathnames = '/api/public/'

export default eventHandler(async (event) => {
  // console.log()
  // cf : https://www.jsdocs.io/package/h3#H3Event
  // console.log('api middleware > auth > event.path :', event.path)
  // console.log('api middleware > auth > event.method :', event.method)

  // console.log('api middleware > auth > event.node.req :', event.node.req)
  // console.log('api middleware > auth > event.context :', event.context)
  // console.log('api middleware > auth > event.headers :', event.headers)

  // const requestedURL = getRequestURL(event)
  // console.log('api middleware > auth > requestedURL :', requestedURL)
  // console.log('api middleware > auth > requestedURL.pathname :', requestedURL.pathname)

  // const name = getRouterParam(event, 'name')
  // console.log('api middleware > auth > name :', name)

  const session = await getServerSession(event) // ) as SessionClient
  console.log('api middleware > auth > session :', session)

  // Add last subscription to event context
  if (session) {
    const lastSubscription = (await getLastActiveSubscriptionByUserId((session as SessionClient).userId)) as UserSubscription
    // console.log('api middleware > auth > lastSubscription :', lastSubscription)
    // event.context.subscription = lastSubscription

    const subscriptionInfos: UserSubscription = {
      stripeProductKey: lastSubscription?.stripeProductKey || '',
      // subscriptionCategory: lastSubscription?.subscriptionCategory || SubscriptionCategoryForPrisma.FREE,
      subscriptionCategory: lastSubscription?.subscriptionCategory || SubscriptionCategory.FREE,
      numberUsers: lastSubscription?.numberUsers || 1,
      numberDatasets: lastSubscription?.numberDatasets || 1,
      numberWidgets: lastSubscription?.numberWidgets || 1
    }
    event.context.subscription = subscriptionInfos
  }

  // Protect api paths not public
  if (
    event.path.startsWith('/api') && //
    !event.path.startsWith('/api/auth') &&
    !event.path.startsWith('/api/stripe/prices') &&
    !event.path.startsWith('/api/stripe/webhooks') &&
    !event.path.startsWith(publicPathnames) &&
    !session
  ) {
    console.log('error at auth middleware')
    throw createError({
      statusMessage: 'Unauthenticated',
      statusCode: 403
    })
  }
})
