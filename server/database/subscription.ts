import type { Subscription } from '~/utils'

/**
 * @desc Get subscriptions by user id
 * @param userId User's id
 */
export async function getSubscriptionsByUserId(userId: string) {
  const subscriptions = await prisma.subscription.findMany({
    where: {
      userId
    }
  })
  return subscriptions
}

/**
 * @desc Get last active subscription by user id
 * @param userId User's id
 */
export async function getLastActiveSubscriptionByUserId(userId: string) {
  const subscriptions = await prisma.subscription.findMany({
    where: {
      userId,
      stripeStatus: 'active'
    },
    orderBy: {
      lastEventDate: 'desc'
    },
    take: 1
  })
  return subscriptions[0] || undefined
}

/**
 * @desc Get subscription by stripeId
 * @param stripeId Subscription's id
 */
export async function getSubscriptionById(stripeId: string) {
  return await prisma.subscription.findFirst({
    where: {
      stripeId
    }
  })
}

/**
 * @desc Create a subscription
 * @param data Subscription's data
 */
export async function createOrUpdateSubscription(data: Subscription) {
  return await prisma.subscription.upsert({
    where: {
      stripeId: data.stripeId
    },
    create: {
      userId: data.userId,
      profileId: data.profileId,
      organisationId: data.organisationId,
      subscriptionCategory: data.subscriptionCategory,
      stripeId: data.stripeId,
      stripeStatus: data.stripeStatus,
      stripeProductKey: data.stripeProductKey,
      stripeProductNickName: data.stripeProductNickName,
      stripePriceId: data.stripePriceId,
      quantity: data.quantity,
      amount: data.amount,
      currency: data.currency,
      trialEndsAt: data.trialEndsAt,
      endsAt: data.endsAt,
      lastEventDate: data.lastEventDate,
      startDate: data.startDate,
      numberUsers: data.numberUsers,
      numberDatasets: data.numberDatasets,
      numberWidgets: data.numberWidgets
    },
    update: {
      organisationId: data.organisationId,
      subscriptionCategory: data.subscriptionCategory,
      stripeStatus: data.stripeStatus,
      stripeProductKey: data.stripeProductKey,
      stripeProductNickName: data.stripeProductNickName,
      stripePriceId: data.stripePriceId,
      quantity: data.quantity,
      amount: data.amount,
      currency: data.currency,
      trialEndsAt: data.trialEndsAt,
      endsAt: data.endsAt,
      lastEventDate: data.lastEventDate,
      startDate: data.startDate,
      numberUsers: data.numberUsers,
      numberDatasets: data.numberDatasets,
      numberWidgets: data.numberWidgets
    }
  })
}
