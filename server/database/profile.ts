import type { BodyProfile, BodyApiProfile } from '~/utils'

// GETTERS

/**
 * @desc Get profile by user id
 * @param userId User's id
 */
export async function getProfileByUserId(userId: string) {
  const profile = await prisma.profile.findFirst({
    where: {
      userId
    }
  })
  return profile
}

// CREATE / UPDATE

/**
 * @desc Create profile by user id
 * @param userId User's id
 * @param email User's email
 * @param bio User's bio
 * @param locale User's locale
 * @param githubLogin User's githubLogin
 * @param githubUrl User's githubUrl
 * @param githubRepos User's githubRepos
 * @param gitlabLogin User's gitlabLogin
 * @param gitlabUrl User's gitlabUrl
 * @param gitlabRepos User's gitlabRepos
 */
export async function createProfile({
  userId,
  email,
  bio,
  locale,
  githubLogin,
  githubUrl,
  githubRepos,
  gitlabLogin,
  gitlabUrl,
  gitlabRepos
}: BodyApiProfile) {
  const userProfile = await prisma.profile.create({
    data: {
      email,
      bio,
      locale,
      githubLogin,
      githubUrl,
      githubRepos,
      gitlabLogin,
      gitlabUrl,
      gitlabRepos,
      user: { connect: { id: userId } }
    }
  })

  return userProfile
}

/**
 * @desc Update profile by user id
 * @param userId User's id
 * @param bio User's bio
 * @param locale User's locale
 * @param githubLogin User's githubLogin
 * @param githubUrl User's githubUrl
 * @param githubRepos User's githubRepos
 * @param gitlabLogin User's gitlabLogin
 * @param gitlabUrl User's gitlabUrl
 * @param gitlabRepos User's gitlabRepos
 */
export async function updateProfile({
  userId, //
  // email,
  bio,
  locale,
  githubLogin,
  githubUrl,
  githubRepos,
  gitlabLogin,
  gitlabUrl,
  gitlabRepos
}: BodyProfile) {
  const userProfile = await prisma.profile.update({
    where: { userId },
    data: {
      bio,
      locale,
      githubLogin,
      githubUrl,
      githubRepos,
      gitlabLogin,
      gitlabUrl,
      gitlabRepos
    }
  })
  return userProfile
}

/**
 * @desc Upsert profile by user id
 * @param userId User's id
 * @param email User's email
 * @param bio User's bio
 * @param locale User's locale
 * @param githubLogin User's githubLogin
 * @param githubUrl User's githubUrl
 * @param githubRepos User's githubRepos
 * @param gitlabLogin User's gitlabLogin
 * @param gitlabUrl User's gitlabUrl
 * @param gitlabRepos User's gitlabRepos
 */
export async function upsertProfile({
  userId, //
  email,
  bio,
  locale,
  githubLogin,
  githubUrl,
  githubRepos,
  gitlabLogin,
  gitlabUrl,
  gitlabRepos
}: BodyApiProfile) {
  const userProfile = await prisma.profile.upsert({
    where: { userId },
    create: {
      email,
      bio,
      locale,
      githubLogin,
      githubUrl,
      githubRepos,
      gitlabLogin,
      gitlabUrl,
      gitlabRepos,
      user: { connect: { id: userId } }
    },
    update: {
      bio,
      locale,
      githubLogin,
      githubUrl,
      githubRepos,
      gitlabLogin,
      gitlabUrl,
      gitlabRepos
    }
  })
  return userProfile
}
