import type { BodyUser, BodyUserStripe, UserDataBasic, UserDataClient } from '~/utils'

/**
 * @desc Returns user by email
 * @param email User's email
 */
export async function getUserByEmail(email: string) {
  const user = await prisma.user.findFirst({
    where: {
      email
    }
  })
  return user
}

/**
 * @desc Returns user by id
 * @param userId User's id
 */
export async function getUserByUserId(userId: string): Promise<UserDataBasic> {
  const user = await prisma.user.findUnique({
    where: {
      id: userId
    }
  })
  return user as UserDataBasic
}

export async function getUserByStripeCustomerId(stripeCustomerId: string) {
  const user = await prisma.user.findFirst({
    where: { stripeCustomerId },
    select: {
      id: true,
      email: true,
      last_name: true,
      first_name: true,
      stripeCustomerId: true,
      profile: {
        select: {
          id: true
        }
      }
    }
  })

  return user
}

// UPDATES

/**
 * @desc Update user by id
 * @param userId User's id
 */
export async function updateUserInfos({
  userId, //
  /* eslint-disable camelcase */
  first_name,
  /* eslint-disable camelcase */
  last_name
}: BodyUser) {
  const userData = await prisma.user.update({
    where: { id: userId },
    data: {
      /* eslint-disable camelcase */
      first_name,
      /* eslint-disable camelcase */
      last_name
    }
  })
  return userData as UserDataClient
}

/**
 * @desc Update user's Stripe customer id by id
 * @param id User's id
 * @param stripeCustomerId User's id
 */
export async function updateUserStripeCustomerId({ id, stripeCustomerId }: BodyUserStripe) {
  const userData = await prisma.user.update({
    where: { id },
    data: {
      stripeCustomerId
    }
  })
  return userData as UserDataClient
}
