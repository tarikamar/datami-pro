import { type UserSubscription } from '@/utils'

const selectFieldsBasic = {
  id: true,
  meta: true,
  // headers: true,
  gitInfos: true,
  // schemaFile: true,
  // customPropsFile: true,
  isSet: true
  // createdByUserId: true,
  // createdAt: true
}

/**
 * @desc Get datasets by author id
 * @param userId User's id
 */
export async function getDatasetsByAuthorId(userId: string, subscription: UserSubscription) {
  console.log('getDatasetsByAuthorId > subscription : ', subscription)
  const datasets = await prisma.dataset.findMany({
    where: {
      createdByUserId: userId
    },
    select: selectFieldsBasic
  })
  return datasets || []
}

/**
 * @desc Get datasets by user id (many to many)
 * @param userId User's id
 */
export async function getDatasetsByUserId(userId: string, subscription: UserSubscription) {
  console.log('getDatasetsByUserId > subscription : ', subscription)
  const datasets = await prisma.dataset.findMany({
    where: {
      users: {
        some: {
          userId
        }
      }
    },
    select: selectFieldsBasic
  })
  return datasets || []
}
