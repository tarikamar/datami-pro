/**
 * @desc Get accounts by user id
 * @param userId User's id
 */
export async function getAccountsByUserId(userId: string) {
  const accounts = await prisma.account.findMany({
    where: {
      userId
    }
  })
  return accounts
}
