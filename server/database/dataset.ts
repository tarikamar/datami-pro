import { Prisma } from '@prisma/client'
import {
  type BodyCreateDataset, //
  type BodyDatasetNoId, //
  type UserSubscription
  // type Dataset
} from '@/utils'

const selectFieldsBasic = {
  id: true,
  meta: true,
  headers: true,
  items: true,
  // gitInfos: true,
  schemaFile: true,
  customPropsFile: true,
  isSet: true
  // createdByUserId: true,
  // createdAt: true
}

/**
 * @desc Create a dataset
 * @param userId User id
 * @param data Dataset's data
 * @param subscription User's subscription
 */
export async function createDataset(
  {
    userId, //
    data
  }: BodyCreateDataset,
  subscription: UserSubscription
) {
  console.log('createDataset > subscription : ', subscription)

  const metaAsJson = data.meta as Prisma.JsonObject
  const headersAsJson = data.headers as Prisma.JsonArray
  const itemsAsJson = data.items as Prisma.JsonArray
  const gitInfosAsJson = data.gitInfos as Prisma.JsonArray

  const dataset = await prisma.dataset.create({
    data: {
      meta: metaAsJson,
      headers: headersAsJson,
      items: itemsAsJson,
      gitInfos: gitInfosAsJson,

      // many-to-one : Dataset }|---|| User
      author: { connect: { id: userId } },

      // many-to-many : Dataset }|---|{ Users
      users: {
        create: [
          {
            user: { connect: { id: userId } }
          }
        ]
      }
    }
  })
  return dataset
}

/**
 * @desc Get dataset by dataset id
 * @param datasetId Dataset's id
 * @param subscription User's subscription
 */
export async function getDatasetByDatasetId(
  datasetId: string, //
  subscription: UserSubscription
) {
  console.log('getDatasetByDatasetId > subscription : ', subscription)

  const dataset = await prisma.dataset.findFirst({
    where: {
      id: datasetId
    },
    select: selectFieldsBasic
  })
  // console.log('getDatasetByDatasetId > dataset : ', dataset)
  return dataset
}

/**
 * @desc Update a dataset by dataset id
 * @param datasetId Dataset's id
 * @param subscription User's subscription
 * @param data Dataset's data
 */
export async function updateDatasetByDatasetId(
  datasetId: string, //
  subscription: UserSubscription,
  data: BodyDatasetNoId
) {
  console.log('updateDatasetByDatasetId > subscription : ', subscription)
  const dataset = await prisma.dataset.update({
    where: {
      id: datasetId
    },
    data,
    select: selectFieldsBasic
  })
  return dataset
}

/**
 * @desc Delete a dataset by dataset id
 * @param datasetId Dataset's id
 */
export async function deleteDatasetByDatasetId(userId: string, datasetId: string) {
  // Delete relationships : from DatasetsOnUsers table
  // const DatasetsOnUsers =
  await prisma.datasetsOnUsers.delete({
    where: {
      userId_datasetId: { userId, datasetId }
    }
  })
  // console.log('deleteDatasetByDatasetId > DatasetsOnUsers : ', DatasetsOnUsers)

  // Delete dataset : from Dataset table
  // const datasetDeleted =
  await prisma.dataset.delete({
    where: {
      id: datasetId
    }
  })
  // console.log('deleteDatasetByDatasetId > datasetDeleted : ', datasetDeleted)
  return // datasetDeleted
}
