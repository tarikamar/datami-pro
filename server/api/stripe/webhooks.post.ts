import Stripe from 'stripe'
// import sendDefaultErrorResponse from '~/server/app/errors/responses/defaultErrorsResponse'
import { handleSubscriptionChange } from '~/server/app/services/stripeService'
import type { SubscriptionFromStripe } from '~/utils'

export default defineEventHandler(async (event) => {
  const stripeEvent = await readBody<Stripe.Event>(event)
  console.log()
  console.log('api/strip/webhooks.post > stripeEvent : ', stripeEvent)

  const isSubscriptionEvent = stripeEvent.type.startsWith('customer.subscription')
  console.log('api/strip/webhooks.post > isSubscriptionEvent : ', isSubscriptionEvent)

  if (!isSubscriptionEvent) {
    // return sendDefaultErrorResponse(event, 'Stripe error', 400, `Could not handle ${stripeEvent.type}. No functionality set.`)
    throw createError({
      statusCode: 400,
      statusMessage: `Could not handle ${stripeEvent.type}. No functionality set.`
    })
  }
  const subscription = stripeEvent.data.object as SubscriptionFromStripe
  console.log('api/strip/webhooks.post > subscription : ', subscription)

  await handleSubscriptionChange(subscription, stripeEvent.created)

  return `handled ${stripeEvent.type}.`
})
