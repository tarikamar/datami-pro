import type { BodyStripeProductPrices, StripeProductPrices } from '~/utils'
import { getProductPrices } from '~/server/app/services/stripeService'

export default eventHandler(async (event) => {
  const body: BodyStripeProductPrices[] = await readBody(event)

  const response = await Promise.all(
    body.map(async (product) => {
      const productKey = product.productKey
      const prices = await getProductPrices(productKey)
      const res = {
        productKey,
        prices
      }
      // console.log('prices.post > res :', res)
      return res
    })
  )

  // console.log('prices.post > response :', response)
  return response as StripeProductPrices[]
})
