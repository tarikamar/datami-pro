import type { BodySubscription, BodyUserStripe } from '~/utils'

import { getSubscribeUrl } from '~/server/app/services/stripeService'
import { getUserByUserId, updateUserStripeCustomerId } from '~/server/database'
// import sendDefaultErrorResponse from '~/server/app/errors/responses/defaultErrorsResponse'

export default eventHandler(async (event) => {
  const { userId, lookupKey }: BodySubscription = await readBody(event)
  console.log('subscribe.post > userId :', userId)
  console.log('subscribe.post > lookupKey :', lookupKey)

  if (!userId || !lookupKey) {
    throw createError({
      statusCode: 500,
      statusMessage: '??? Info'
    })
  }

  const user = await getUserByUserId(userId)
  console.log('subscribe.post > user :', user)

  const { user: customer, clientSecret, shouldUpdateUser, error } = await getSubscribeUrl(lookupKey, user)
  console.log('subscribe.post > customer :', customer)
  console.log('subscribe.post > clientSecret :', clientSecret)
  console.log('subscribe.post > shouldUpdateUser :', shouldUpdateUser)

  if (error) {
    throw createError({
      statusCode: error.statusCode,
      statusMessage: error.message
    })
  }

  if (shouldUpdateUser) {
    await updateUserStripeCustomerId(customer as BodyUserStripe)
  }
  // await sendRedirect(event, url) // NOT WORKING ON LOCALHOST
  return { clientSecret, error }
})
