import { PrismaAdapter } from '@next-auth/prisma-adapter'
import EmailProvider from 'next-auth/providers/email'
import GithubProvider from 'next-auth/providers/github'
import type { GithubProfile } from 'next-auth/providers/github'
import GitlabProvider from 'next-auth/providers/gitlab'
import type { GitLabProfile } from 'next-auth/providers/gitlab'
import CredentialsProvider from 'next-auth/providers/credentials'
// import { generate } from 'generate-password'
import { encode, decode } from 'next-auth/jwt'
import { NuxtAuthHandler } from '#auth'

import { nodemailerConfig, customVerificationEmail } from '~/server/app/services/mailer'

// Queries functions
import { getUserByEmail } from '~/server/database/user'
import { upsertProfile } from '~/server/database/profile'

const config = useRuntimeConfig()
// console.log('config : ', config)
const prismaAdapter = PrismaAdapter(prisma)

// --------------------------
// NUXT AUTH HANDLER
// --------------------------

export default NuxtAuthHandler({
  secret: process.env.AUTH_SECRET,
  debug: false,
  // debug: process.env.NODE_ENV !== 'production', // for testing purposes
  pages: {
    // Change the default behavior to use `/login` as the path for the sign-in page
    signIn: '/account/login',
    signOut: '/account/login',
    // verifyRequest: `/account/verify`,
    error: '/error'
  },
  // cf : https://authjs.dev/reference/adapter/prisma
  adapter: prismaAdapter,
  session: {
    // Choose how you want to save the user session.
    // The default is `"jwt"`, an encrypted JWT (JWE) stored in the session cookie.
    // If you use an `adapter` however, we default it to `"database"` instead.
    // You can still force a JWT session by explicitly defining `"jwt"`.
    // When using `"database"`, the session cookie will only contain a `sessionToken` value,
    // which is used to look up the session in the database.
    // cf : https://nneko.branche.online/next-auth-credentials-provider-with-the-database-session-strategy/
    strategy: 'database'
    // strategy: 'jwt'
  },
  providers: [
    // @ts-expect-error You need to use .default here for it to work during SSR. May be fixed via Vite at some point
    EmailProvider.default({
      id: 'send',
      type: 'email',
      name: 'magicLink',
      server: nodemailerConfig,
      from: config.EMAIL_FROM,
      sendVerificationRequest({ identifier: to, url, provider: { server, from } }: any) {
        customVerificationEmail(to, url, server, from)
      }
    }),
    // @ts-expect-error You need to use .default here for it to work during SSR. May be fixed via Vite at some point
    GithubProvider.default({
      clientId: config.public.GITHUB_CLIENT_ID,
      clientSecret: config.GITHUB_CLIENT_SECRET,
      profile(profile: GithubProfile) {
        // console.log('GithubProvider > profile', profile)
        // const pwd = generate(pwdConf)
        return {
          id: profile.id.toString(),
          // uuid: makeUuid()
          name: profile.name ?? profile.login,
          first_name: profile.name ?? profile.login,
          last_name: profile.name ?? profile.login,
          email: profile.email,
          image: profile.avatar_url
          // password: await hashPassword(pwd)
        }
      }
    }),
    // @ts-expect-error You need to use .default here for it to work during SSR. May be fixed via Vite at some point
    GitlabProvider.default({
      clientId: config.public.GITLAB_CLIENT_ID,
      clientSecret: config.GITLAB_CLIENT_SECRET,
      profile(profile: GitLabProfile) {
        // console.log('GitlabProvider > profile', profile)
        // const pwd = generate(pwdConf)
        return {
          id: profile.id.toString(),
          // uuid: makeUuid(),
          name: profile.name ?? profile.username,
          first_name: profile.name ?? profile.login,
          last_name: profile.name ?? profile.login,
          email: profile.email,
          image: profile.avatar_url
          // password: await hashPassword(pwd)
        }
      }
    }),
    // NOT IN USE - PASSWORDLESS APPROACH
    // @ts-expect-error
    CredentialsProvider.default({
      name: 'credentials',
      credentials: {},
      async authorize(credentials: { email: string; password: string }, req: any) {
        console.log('authorize > email : ', credentials.email)
        console.log('authorize > password : ', credentials.password)
        console.log('authorize > req : ', req)

        // Check credentials (request body)
        if (!credentials.email || !credentials.password)
          return createError({
            statusCode: 401,
            statusMessage: 'No email or password provided'
          })

        // Fetch user from database
        const user = await getUserByEmail(credentials.email)
        console.log('authorize > user : ', user)
        if (!user) {
          throw createError({
            statusCode: 403,
            statusMessage: 'Credentials not working'
          })
        }

        // Verify password
        const passwordIsVerified = user.password && (await verifyPassword(user.password, credentials.password))
        console.log('authorize > passwordIsVerified : ', passwordIsVerified)

        if (!passwordIsVerified) {
          throw createError({
            statusCode: 401,
            statusMessage: 'Invalid login'
          })
        }

        return user
      }
    })
  ],

  events: {
    // cf https://next-auth.js.org/configuration/events
    // linkAccount: ({ user, account, profile }) => {
    //   console.log()
    //   console.log('events > linkAccount > user :', user)
    //   console.log('events > linkAccount > account (from provider) :', account)
    //   console.log('events > linkAccount > profile (from proviver):', profile)
    // },
    signIn: async ({
      user, //
      account,
      // profile,
      isNewUser
    }) => {
      console.log()
      console.log('events > signIn > isNewUser :', isNewUser)
      // console.log('events > signIn > user :', user)
      console.log('events > signIn > account (from provider + callback) :', account)
      // console.log('events > signIn > profile (from provider):', profile)

      // User is now supposed to exists in DB's User table and in Account table
      // We should be able to update automatically its Profile table
      const dbProfile = await upsertProfile({
        userId: user.id,
        email: user.email as string,
        githubLogin: account?.githubLogin as string,
        githubUrl: account?.githubUrl as string,
        githubRepos: account?.githubRepos as string,
        gitlabLogin: account?.gitlabLogin as string,
        gitlabUrl: account?.gitlabUrl as string,
        gitlabRepos: account?.gitlabRepos as string
      })
      console.log('events > signIn > dbProfile :', dbProfile)
    }
  },

  callbacks: {
    // @ts-ignore
    signIn({
      account, //
      // user,
      profile
    }) {
      // console.log()
      // console.log('callbacks > signIn > user : ', user)
      // console.log('callbacks > signIn > account : ', account)
      // console.log('callbacks > signIn > profile : ', profile)

      // Append proxy custom infos to account
      // Usefull in
      if (account?.provider === 'github') {
        const profileGithub = profile as GithubProfile
        account.githubLogin = profileGithub.login
        // account.githubUrl = profileGithub.url
        // account.githubRepos = profileGithub.repos_url
        account.githubUrl = `https://github.com/${profileGithub.name}`
        account.githubRepos = `https://github.com/${profileGithub.name}?tab=repositories`
      }
      if (account?.provider === 'gitlab') {
        const profileGitlab = profile as GitLabProfile
        account.gitlabLogin = profileGitlab.name
        account.gitlabUrl = `https://gitlab.com/${profileGitlab.name}`
        account.gitlabRepos = `https://gitlab.com/users/${profileGitlab.name}/projects`
      }

      return true
    },

    // redirect({ url, baseUrl }) {
    //   console.log()
    //   console.log('redirect > url : ', url)
    //   console.log('redirect > baseUrl : ', baseUrl)
    //   return url
    // },

    // Specify here the payload of your token and session
    jwt({
      token, //
      user,
      // profile,
      // session,
      account
    }: any) {
      // console.log()
      // console.log('jwt > user : ', user)
      // console.log('jwt > account : ', account)
      // console.log('jwt > session : ', session)
      // console.log('jwt > profile : ', profile)
      // console.log('jwt > token - A : ', token)

      if (user) {
        token.id = user.id
        token.name = user.first_name || user.name
        token.email = user.email
        token.type = account.type
        token.accessToken = account.access_token
      }
      // console.log('jwt > token - B : ', token)

      return token
    },

    session({
      session, //
      // token,
      user
    }: any) {
      // console.log()
      // console.log('session > session - A : ', session)
      // console.log('session > user : ', user)

      session.userId = user.id
      // console.log('session > session - B : ', session)

      return session
    }
  },

  jwt: {
    encode: async ({ token, secret, maxAge }) => {
      // console.log('jwt > encode > token : ', token)
      return await encode({ token, secret, maxAge }) // <<<<<<   This needed to be wrapped with braces
    },
    decode: async ({ token, secret }) => {
      // console.log('jwt > decode > token : ', token)
      return await decode({ token, secret }) //  <<<<<  This needed to be wrapped with braces
    }
  }
})
