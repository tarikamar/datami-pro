// import { H3Error } from 'h3'
import { hash } from 'argon2'
import type { NewUser } from '~/utils'

export default defineEventHandler(async (event) => {
  const body: NewUser = await readBody(event)
  if (!body.email || !body.password) {
    throw createError({
      statusCode: 500,
      statusMessage: 'Missing Info'
    })
  }

  const userExists = await prisma.user.findFirst({
    where: {
      email: body.email
    }
  })
  if (userExists) {
    throw createError({
      statusCode: 403,
      statusMessage: 'User already exists'
    })
  }

  const hashedPassword: string = await hash(body.password)

  const user = await prisma.user.create({
    data: {
      email: body.email,
      first_name: body.first_name?.trim() || '',
      last_name: body.last_name?.trim() || '',
      // uuid: makeUuid(),
      password: hashedPassword
    }
  })

  console.log('register.post > user: ', user)

  // Link account
  const account = await prisma.account.create({
    data: {
      userId: user.id,
      // email: body.email,
      type: 'credentials',
      provider: 'credentials',
      providerAccountId: '99'
    }
  })

  setResponseStatus(event, 201)

  return {
    user,
    account,
    message: 'User created'
  }
})
