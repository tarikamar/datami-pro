export default defineEventHandler(async (event) => {
  const { username } = getQuery(event)
  // console.log('api > gitlab.get > username :', username)
  // console.log('api > gitlab.get > event :', event)

  const config = useRuntimeConfig()
  // console.log('api > gitlab.get > GITLAB_CLIENT_ID :', GITLAB_CLIENT_ID)

  const products = await $fetch('https://fakestoreapi.com/products')

  return {
    GitlabClientId: config.public.GITLAB_CLIENT_ID,
    GithubClientId: config.public.GITHUB_CLIENT_ID,
    username,
    products
  }
})
