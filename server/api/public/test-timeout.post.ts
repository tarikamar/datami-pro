const fakeData = [{ key: 'testTimeout', value: 'value' }]

export default defineEventHandler(async () => {
  return new Promise<any>((resolve) => {
    setTimeout(() => {
      resolve(fakeData)
    }, 2000)
  })
})
