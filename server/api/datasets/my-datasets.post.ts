import { type BodyUser } from '~/utils'
import { getDatasetsByUserId } from '@/server/database'

// Get Datasets an user is part of
export default eventHandler(async (event) => {
  const body: BodyUser = await readBody(event)

  const userSubscription = event.context.subscription
  console.log('my-datasets.post > userSubscription : ', userSubscription)

  if (!body.userId) {
    throw createError({
      statusCode: 500,
      statusMessage: 'Missing Info'
    })
  }

  // return await getDatasetsByUserId(body.userId, userSubscription)
  return new Promise<any>((resolve) => {
    resolve(getDatasetsByUserId(body.userId, userSubscription))
  })
})
