import type { BodyUser } from '~/utils'
import { getSubscriptionsByUserId } from '~/server/database'

export default eventHandler(async (event) => {
  const body: BodyUser = await readBody(event)

  if (!body.userId) {
    throw createError({
      statusCode: 500,
      statusMessage: 'Missing Info'
    })
  }

  return await getSubscriptionsByUserId(body.userId)
})
