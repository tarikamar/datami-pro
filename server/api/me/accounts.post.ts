import type { BodyAccount } from '~/utils'
import { getAccountsByUserId } from '~/server/database'

export default eventHandler(async (event) => {
  const body: BodyAccount = await readBody(event)

  if (!body.userId) {
    throw createError({
      statusCode: 500,
      statusMessage: 'Missing Info'
    })
  }

  const profile = await getAccountsByUserId(body.userId)

  return profile
})
