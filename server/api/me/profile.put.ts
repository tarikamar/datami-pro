import type { BodyProfile } from '~/utils'
import { updateProfile } from '~/server/database'

export default eventHandler(async (event) => {
  const body: BodyProfile = await readBody(event)
  // console.log('api/me/profile.put > body : ', body)

  if (!body.userId) {
    throw createError({
      statusCode: 500,
      statusMessage: 'Missing Info'
    })
  }

  const profile = await updateProfile(body)

  return profile
})
