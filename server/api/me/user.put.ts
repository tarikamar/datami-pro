import type { BodyUser } from '~/utils'
import { updateUserInfos } from '@/server/database/user'

export default eventHandler(async (event) => {
  const body: BodyUser = await readBody(event)

  if (!body.userId) {
    throw createError({
      statusCode: 500,
      statusMessage: 'Missing Info'
    })
  }

  const user = await updateUserInfos(body)

  return user
})
