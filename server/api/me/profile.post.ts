import type { BodyProfile } from '~/utils'
import { getProfileByUserId } from '~/server/database'

export default eventHandler(async (event) => {
  const body: BodyProfile = await readBody(event)

  if (!body.userId) {
    throw createError({
      statusCode: 500,
      statusMessage: 'Missing Info'
    })
  }

  return await getProfileByUserId(body.userId)
})
