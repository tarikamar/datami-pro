import type { BodyProfile } from '~/utils'
import { getUserByUserId } from '~/server/database'

export default eventHandler(async (event) => {
  const body: BodyProfile = await readBody(event)

  if (!body.userId) {
    throw createError({
      statusCode: 500,
      statusMessage: 'Missing Info'
    })
  }

  const user = await getUserByUserId(body.userId)

  return user
})
