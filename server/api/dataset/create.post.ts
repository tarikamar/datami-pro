import type { BodyCreateDataset } from '~/utils'
import { createDataset } from '~/server/database'

export default eventHandler(async (event) => {
  const body: BodyCreateDataset = await readBody(event)
  console.log('my-datasets.post > body : ', body)

  const userSubscription = event.context.subscription
  console.log('my-datasets.post > userSubscription : ', userSubscription)

  if (!body.userId) {
    throw createError({
      statusCode: 500,
      statusMessage: 'Missing Info'
    })
  }
  const dataset = await createDataset(body, userSubscription)
  return dataset
})
