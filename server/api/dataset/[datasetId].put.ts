import type { BodyUpdateDataset } from '~/utils'
import { updateDatasetByDatasetId } from '~/server/database'

export default defineEventHandler(async (event) => {
  const datasetId = getRouterParam(event, 'datasetId')

  const body: BodyUpdateDataset = await readBody(event)
  if (!body.userId || !datasetId) {
    throw createError({
      statusCode: 500,
      statusMessage: 'Missing Info'
    })
  }

  const userSubscription = event.context.subscription
  console.log('[datasetId].post > userSubscription : ', userSubscription)

  // return `put, ${datasetId}!`
  return await updateDatasetByDatasetId(datasetId, userSubscription, body.data)
})
