import type { BodyUser } from '~/utils'
import { getDatasetByDatasetId } from '~/server/database'

export default defineEventHandler(async (event) => {
  const body: BodyUser = await readBody(event)
  const datasetId = getRouterParam(event, 'datasetId')

  const userSubscription = event.context.subscription
  console.log('[datasetId].post > userSubscription : ', userSubscription)

  if (!body.userId || !datasetId) {
    throw createError({
      statusCode: 500,
      statusMessage: 'Missing Info'
    })
  }

  // TO DO : check if user has the right to access the dataset

  // TO DO : get queries such as pagination / filtering
  const query = getQuery(event)
  console.log('[datasetId].post > query : ', query)

  return await getDatasetByDatasetId(datasetId, userSubscription)
})
