import type { BodyDeleteDataset } from '~/utils'
import { deleteDatasetByDatasetId } from '~/server/database'

export default defineEventHandler(async (event) => {
  const body: BodyDeleteDataset = await readBody(event)
  console.log('dataset/delete > body : ', body)

  const userSubscription = event.context.subscription
  console.log('dataset/delete > userSubscription : ', userSubscription)

  if (!body.userId || !body.datasetId) {
    throw createError({
      statusCode: 500,
      statusMessage: 'Missing Info'
    })
  }

  // TO DO : check if user has the right to delete the dataset

  return await deleteDatasetByDatasetId(body.userId, body.datasetId)
})
