import type { PropertyPath } from '@/utils'

// --------------------
// CONSTANTS
// --------------------

export const stringTrueValues: string[] = [
  'yes', //
  'y',
  'true',
  '1',
  'oui',
  'vrai'
]

// --------------------
// REQUESTS
// --------------------

// export const debounce = (func: Function, timeout: number = 500) => {
//   let timeoutId: NodeJS.Timeout
//   return (...args: any[]) => {
//     clearTimeout(timeoutId)
//     timeoutId = setTimeout(() => {
//       func(...args)
//     }, timeout)
//   }
// }
// export function debounce (func: Function, timeout = 500) {
//   // cf : https://stackoverflow.com/questions/42199956/how-to-implement-debounce-in-vue2
//   // console.log('\nU > globalUtils > debounce > fn : ', fn)
//   // console.log('U > globalUtils > debounce > delay : ', delay)
//   let timeoutID : NodeJS.Timeout
//   return (...args: any[]) =>  {
//     clearTimeout(timeoutID)
//     timeoutID = setTimeout(() => {
//       func.apply( this, ...args)
//     }, timeout)
//   }
// }

// --------------------
// SETTERS
// --------------------

export const setIn = (obj: any, [head, ...rest]: string[], value: any) => {
  const newObj: any = Array.isArray(obj) ? [...(obj as [])] : { ...obj }
  newObj[head] = rest.length ? setIn(obj[head], rest, value) : value
  return newObj
}

export const setProperty = (obj: object, path: string, value: any) => {
  let resultObj: object
  if (path === '.') {
    resultObj = { ...obj, ...value } as object
  } else {
    const pathAsArray = path.split('.')
    resultObj = setIn(obj, pathAsArray, value)
  }
  return resultObj
}

// --------------------
// GETTERS
// --------------------

export const getFrom = (from: any, selectors: PropertyPath[]) => {
  const res = selectors.map((selector: PropertyPath) => {
    const arraySelectors = Array.isArray(selector)
      ? selector
      : selector
          // Add support for array notation, e.g.
          // a.b[3] becomes a.b.3.
          .replace(/\[([^[\]]*)\]/g, '.$1.')
          .split('.')
          .filter((t: any) => t !== '')

    return getFromArraySelectors(from, arraySelectors)
  })
  return res
}

const getFromArraySelectors = (from: any, selectors: string[]) => {
  return selectors.reduce((prev: any, cur: any) => {
    return prev && prev[cur]
  }, from)
}

export const getFromOnePath = (from: any, selector: string) => {
  // console.log('getFromOnePath > from : ', from)
  // console.log('getFromOnePath > selector : ', selector)
  const val = getFrom(from, [selector])
  // console.log('getFromOnePath > val : ', val)
  return val[0]
}

export const findInObjectsArray = (objectsArray: object[], id: string, all: boolean = false) => {
  const arrayFlat: Record<string, unknown> = Object.assign({}, ...objectsArray) as Record<string, unknown>
  return all ? arrayFlat : arrayFlat[id]
}

export const groupBy = <T>(objectsArray: T[], key: keyof T): Record<string, T[]> => {
  return objectsArray.reduce((rv: Record<string, T[]>, x: T) => {
    const keyValue = x[key] as unknown as string
    ;(rv[keyValue] = rv[keyValue] || []).push(x)
    return rv
  }, {})
}

export const injectInObject = (value: object | object[], target: object) => {
  const targetObject = target || {}
  let valueOut: object = { ...targetObject }
  if (Array.isArray(value)) {
    value.forEach((v) => {
      valueOut = { ...valueOut, ...v }
    })
  } else {
    valueOut = { ...valueOut, ...value }
  }
  return valueOut
}
