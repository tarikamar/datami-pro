import { v4 as uuidv4 } from 'uuid'

import { TableAction, type TableHeaderHelper, LocalItem } from '@/utils/types'

export const defaultHeaderLeft: TableHeaderHelper = {
  id: 'header-left',
  name: 'header-left',
  actions: [
    TableAction.handleRow, //
    TableAction.selectRow
    // TableAction.editRow
  ]
}
export const defaultHeaderRight: TableHeaderHelper = {
  id: 'header-right',
  name: '',
  actions: [
    TableAction.removeRow //
  ]
}

export const headerTypes = Object.entries(TableHeaderType).map(([key, value]) => ({ key, value }))
export const headerSubTypes = Object.entries(TableHeaderSubtype).map(([key, value]) => ({ key, value }))
export const headerSubTypesString = headerSubTypes.filter((t) => !['geopoint', 'percent'].includes(t.key))
export const headerSubTypesNumber = headerSubTypes.filter((t) => ['geopoint', 'percent'].includes(t.key))

export const headersSettings: InputField[] = [
  {
    table: InputTable.datasets,
    key: 'tableSchema.title',
    inputType: InputType.text,
    inputLocal: LocalItem.headers
  },
  {
    table: InputTable.datasets,
    key: 'tableSchema.description',
    inputType: InputType.textarea,
    inputLocal: LocalItem.headers
  },
  {
    table: InputTable.datasets,
    key: 'tableSchema.type',
    inputType: InputType.select,
    inputLocal: LocalItem.headers,
    options: {
      select: headerTypes,
      selectKey: 'key',
      selectValue: 'value'
    }
  },
  {
    table: InputTable.datasets,
    key: 'customProps.subtype',
    inputType: InputType.select,
    inputLocal: LocalItem.headers,
    options: {
      select: headerSubTypes,
      selectKey: 'key',
      selectValue: 'value'
    }
  }
]

export const headersSettingsTableSchema: InputField[] = [
  {
    table: InputTable.datasets,
    key: 'name',
    inputType: InputType.text,
    inputLocal: LocalItem.headers
  },
  {
    table: InputTable.datasets,
    key: 'tableSchema.title',
    inputType: InputType.text,
    inputLocal: LocalItem.headers
  },
  {
    table: InputTable.datasets,
    key: 'tableSchema.type',
    inputType: InputType.select,
    inputLocal: LocalItem.headers,
    options: {
      select: headerTypes,
      selectKey: 'key',
      selectValue: 'value'
    }
  },
  {
    table: InputTable.datasets,
    key: 'tableSchema.description',
    inputType: InputType.textarea,
    inputLocal: LocalItem.headers
  },
  {
    table: InputTable.datasets,
    key: 'tableSchema.constraints.enum',
    inputType: InputType.combobox,
    inputLocal: LocalItem.headers,
    allowNew: true
  },
  {
    table: InputTable.datasets,
    key: 'tableSchema.format',
    inputType: InputType.text,
    inputLocal: LocalItem.headers
  },
  {
    table: InputTable.datasets,
    key: 'tableSchema.example',
    inputType: InputType.textarea,
    inputLocal: LocalItem.headers
  }
]

export const headersSettingsCustomProps: InputField[] = [
  {
    table: InputTable.datasets,
    key: 'customProps.subtype',
    inputType: InputType.combobox,
    inputLocal: LocalItem.headers,
    options: {
      select: headerSubTypes,
      selectKey: 'key',
      selectValue: 'value'
    }
  },
  {
    table: InputTable.datasets,
    key: 'customProps.tagSeparator',
    inputType: InputType.select,
    inputLocal: LocalItem.headers,
    allowNew: true,
    options: {
      select: [{ key: '-', value: '-' }],
      selectKey: 'key',
      selectValue: 'value'
    }
  },
  {
    table: InputTable.datasets,
    key: 'customProps.separator',
    inputType: InputType.combobox,
    inputLocal: LocalItem.headers,
    allowNew: true,
    options: {
      select: [{ key: '-', value: '-' }],
      selectKey: 'key',
      selectValue: 'value'
    }
  },
  {
    table: InputTable.datasets,
    key: 'customProps.sticky',
    inputType: InputType.switch,
    inputLocal: LocalItem.headers
  },
  {
    table: InputTable.datasets,
    key: 'customProps.hide',
    inputType: InputType.switch,
    inputLocal: LocalItem.headers
  },
  {
    table: InputTable.datasets,
    key: 'customProps.allowNew',
    inputType: InputType.switch,
    inputLocal: LocalItem.headers
  },
  {
    table: InputTable.datasets,
    key: 'customProps.primaryKey',
    inputType: InputType.checkbox,
    inputLocal: LocalItem.headers
  },
  {
    table: InputTable.datasets,
    key: 'customProps.bgColor',
    inputType: InputType.text,
    inputLocal: LocalItem.headers
  },
  {
    table: InputTable.datasets,
    key: 'customProps.foreignKey',
    inputType: InputType.text,
    inputLocal: LocalItem.headers
  },
  {
    table: InputTable.datasets, // cf CustomPropDefinition
    key: 'customProps.definitions',
    inputType: InputType.combobox,
    inputLocal: LocalItem.headers
  },
  {
    table: InputTable.datasets,
    key: 'customProps.maxLength',
    inputType: InputType.number,
    inputLocal: LocalItem.headers
  },
  {
    table: InputTable.datasets,
    key: 'customProps.longtextOptions',
    inputType: InputType.textarea,
    inputLocal: LocalItem.headers
  },
  {
    table: InputTable.datasets,
    key: 'customProps.stepOptions',
    inputType: InputType.textarea,
    inputLocal: LocalItem.headers
  }
]

// inpired by : https://codepen.io/crwilson311/pen/Bajbdwd
export const resizableGrid = (table: HTMLTableElement, ref?: any) => {
  const row = table.getElementsByTagName('tr')[0]
  const cols = row ? row.children : undefined
  if (!cols) return

  table.style.overflow = 'hidden'

  const tableHeight = table.offsetHeight
  const exceptions = ['header-left', 'header-right']

  for (const col of cols) {
    const div = createDiv(tableHeight)
    col.appendChild(div)
    // col.style.position = 'relative'
    // console.log(col)
    setListeners(div)
  }

  function setListeners(div: HTMLDivElement) {
    let pageX: number | undefined, //
      curCol: HTMLElement | undefined,
      curColWidth: number | undefined
    // nxtCol: HTMLElement | undefined,
    // nxtColWidth: number | undefined

    div.addEventListener('mousedown', function (e) {
      curCol = (e.target as HTMLDivElement)?.parentElement as HTMLElement
      if (curCol && !exceptions.includes(curCol.id)) {
        // nxtCol = curCol?.nextElementSibling as HTMLElement
        pageX = e.pageX
        const padding = paddingDiff(curCol)
        curColWidth = curCol.offsetWidth - padding
        // if (nxtCol) nxtColWidth = nxtCol.offsetWidth - padding
      }
    })

    div.addEventListener('mouseover', function (e) {
      curCol = (e.target as HTMLDivElement)?.parentElement as HTMLElement
      if (curCol && !exceptions.includes(curCol.id)) {
        ;(e.target as HTMLElement).style.borderRight = '3px solid #fc8d59ff'
        ;(e.target as HTMLElement).style.cursor = 'col-resize'
      }
    })

    div.addEventListener('mouseout', function (e) {
      ;(e.target as HTMLElement).style.borderRight = ''
    })

    document.addEventListener('mousemove', function (e) {
      // console.log(e)
      if (curCol && !exceptions.includes(curCol.id)) {
        const diffX = e.pageX - (pageX as number)
        // if (nxtCol) {
        //   nxtCol.style.minWidth = (nxtColWidth as number) - diffX + 'px'
        // }
        let newWidth = (curColWidth as number) + diffX
        newWidth = newWidth < 100 ? 100 : newWidth
        curCol.style.minWidth = newWidth + 'px'
        // console.log('curCol.id : ', curCol.id)

        if (!ref.value.headers[curCol.id]) {
          ref.value.headers[curCol.id] = { width: newWidth }
        } else {
          ref.value.headers[curCol.id].width = newWidth
        }
      }
    })

    document.addEventListener('mouseup', function () {
      curCol = undefined
      // nxtCol = undefined
      pageX = undefined
      // nxtColWidth = undefined
      curColWidth = undefined
    })
  }

  function createDiv(height: number) {
    const div = document.createElement('div')
    div.style.top = '0px'
    div.style.right = '0px'
    div.style.width = '4px'
    div.style.position = 'absolute'
    // div.style.cursor = 'col-resize'
    div.style.userSelect = 'none'
    div.style.height = height + 'px'
    return div
  }

  function paddingDiff(col: HTMLElement) {
    if (getStyleVal(col, 'box-sizing') === 'border-box') {
      return 0
    }
    const padLeft = getStyleVal(col, 'padding-left')
    const padRight = getStyleVal(col, 'padding-right')
    return parseInt(padLeft) + parseInt(padRight)
  }

  function getStyleVal(elm: HTMLElement, css: string) {
    return window.getComputedStyle(elm, null).getPropertyValue(css)
  }
}

export const buildNewHeader = () => {
  const headerId = uuidv4()
  const newHeader: TableHeader = {
    id: headerId,
    name: 'New header',
    tableSchema: {
      title: 'New header',
      type: TableHeaderType.string
    }
  }
  return newHeader
}
export const buildNewRow = (headers?: TableHeader[]) => {
  // console.log('headers : ', headers)
  const newItem: TableItem = { id: uuidv4(), data: {} }
  if (headers) {
    headers.forEach((h) => {
      newItem[h.id] = null
    })
  }
  return newItem
}
