export type SessionData = {
  id: string
  // userId: string
  // start_time: Date
  // end_time?: Date
  // access_token: string
  // csrf_token: string
  // is_active: boolean
  // ip_address: string
  sessionToken: string
  userId: string
  expires?: Date

  // CUSTOM
  // email     String?
  createdAt: Date
}

export type SessionClient = {
  userId: string
  expires: string
  user: {
    name: string
    email: string
    image?: string
  }
}
