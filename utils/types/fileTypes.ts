export enum FileTarget {
  dataset = 'dataset',
  tableSchema = 'table-schema',
  customProps = 'custom-props',
  mapConfig = 'map-config',
  datavizConfig = 'dataviz-config'
}
