export type BasicLink = {
  text: string
  icon: string
  to: string
  disable?: boolean
}

export type ExternalLink = {
  title: string
  subtitle?: string
  url: string
  logo?: string
  icon?: string
}
