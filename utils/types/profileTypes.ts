export type PartialGithubProfile = {
  githubLogin?: string
  githubUrl?: string
  githubRepos?: string
}

export type PartialGitlabProfile = {
  gitlabLogin?: string
  gitlabUrl?: string
  gitlabRepos?: string
}

export type PartialGitProfile = PartialGithubProfile & PartialGitlabProfile

// from client
export type BodyProfile = PartialGitProfile & {
  userId: string
  email?: string
  bio?: string
  locale?: string
}

// from API
export type BodyApiProfile = BodyProfile & {
  email: string
}

// from DB
export type FullProfile = BodyProfile & {
  id: string
  email: string
}
