export enum InputTable {
  datasets = 'datasets',
  widgets = 'widgets'
}

export enum InputType {
  file = 'file',
  fileBlank = 'fileBlank',
  fileCsv = 'fileCsv',
  fileXls = 'fileXls',
  fileJson = 'fileJson',
  fileGeoJson = 'fileGeoJson',
  fileMarkdown = 'fileMarkdown',
  fileText = 'fileText',
  fileImage = 'fileImage',
  fileUrl = 'fileUrl',
  text = 'text',
  tags = 'tags',
  link = 'link',
  links = 'links',
  email = 'email',
  emails = 'emails',
  password = 'password',
  textarea = 'textarea',
  checkbox = 'checkbox',
  number = 'number',
  range = 'range',
  switch = 'switch',
  select = 'select',
  combobox = 'combobox',
  radio = 'radio',
  color = 'color',
  object = 'object',
  array = 'array'
}

export enum Variant {
  outlined = 'outlined',
  filled = 'filled',
  plain = 'plain',
  underlined = 'underlined',
  solo = 'solo',
  soloInverted = 'solo-inverted',
  soloFilled = 'solo-filled'
}

export enum Density {
  default = 'default',
  comfortable = 'comfortable',
  compact = 'compact'
}

export type InputSelectObject = {
  key: string
  value: any
}
export type InputFieldOptions = {
  select?: string[] | InputSelectObject[]
  selectKey?: string
  selectValue?: string
  asIcon?: boolean
}

export enum LocalItem {
  meta = 'meta',
  headers = 'headers',
  items = 'items'
}

export type InputField = {
  table: InputTable
  key: string
  hint?: string
  inputType?: InputType
  inputLocal?: LocalItem
  icon?: string
  variant?: Variant
  density?: Density
  allowNew?: boolean
  // mode?: string
  options?: InputFieldOptions
  model?: any
  readonly?: boolean
}
