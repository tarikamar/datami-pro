export type { LocationQueryValue } from 'vue-router'

export enum InOrOut {
  OUT = 'logout',
  IN = 'login'
}

export interface ProviderData {
  id: string
  name: string
  [name: string]: any
}

export type ProviderUser = {
  id: number
  provider: 'GITHUB'
  provider_user_id: string
  userId: string
}

export type JSONResponse = {
  status: 'success' | 'fail'
  data?: any
  error?: any
}

export type TokensSession = {
  accessToken: string
  refreshToken: string
  sid?: string
}

export type ClientPlatforms = 'app' | 'browser' | 'browser-dev'

export type RefreshTokenData = {
  id: number
  token_id: string
  userId: string
  is_active: boolean
  date_created: Date
}

export type RefreshTokensData = Array<RefreshTokenData>

export interface FlatObject {
  [name: string]: string | string[] | object | object[] | number | number[]
}
