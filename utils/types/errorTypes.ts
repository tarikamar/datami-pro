export type ErrorObj = {
  errorType?: string
  status?: number
  statusCode?: number
  message?: string
  statusMessage?: string
  [name: string]: any | undefined
}
