// ---------------------
// HEADERS
// ---------------------

// FRICTIONLESS
// cf : https://specs.frictionlessdata.io/table-schema/#types-and-formats
export enum TableHeaderType {
  string = 'string',
  integer = 'integer',
  float = 'float',
  number = 'number',
  boolean = 'boolean',
  date = 'date',
  // time = 'time',
  // datetime = 'datetime',
  // year = 'year',
  // yearmonth = 'yearmonth',
  // duration = 'duration',
  // object = 'object',
  // array = 'array',
  // geopoint = 'geopoint',
  // geojson = 'geojson',
  any = 'any'
}

export type TableSchemaConstraints = {
  required?: boolean
  unique?: boolean
  minLength?: number
  minimum?: number | Date
  maximum?: number | Date
  pattern?: string
  enum?: string[]
}

export type TableSchema = {
  type: TableHeaderType
  title?: string
  description?: string
  format?: string
  example?: string
  constraints?: TableSchemaConstraints
}
export type TableSchemaOfficial = TableSchema & {
  name?: string
}
export type TableSchemaOfficialWrapper = {
  fields: TableSchemaOfficial[]
}

// CUSTOM PROPS
export enum TableHeaderSubtype {
  tag = 'tag',
  tags = 'tags',
  longtext = 'longtext',
  link = 'link',
  links = 'links',
  email = 'email',
  emails = 'emails',
  timelinetext = 'timelinetext',
  geopoint = 'geopoint',
  percent = 'percent',
  image = 'image',
  images = 'images'
}

export type CustomPropForeignKey = {
  ressource?: string
  fields?: string
  activate?: boolean
  returnFields?: string[]
}

export type CustomPropDefinition = {
  value: string | number
  label: string
  description?: string
  bgColor?: string
}

export type CustomPropLongTextoptions = {
  breakSeparator?: string
  bulletSeparator?: string
  canCollapse?: boolean
  isOpen?: boolean
}

export type CustomPropStepOptions = {
  stepEnd?: string
  stepSeparator?: string
  stepTitleSeparator?: string
}

export type CustomProp = {
  subtype?: TableHeaderSubtype
  allowNew?: boolean
  sticky?: boolean
  primaryKey?: boolean
  bgColor?: string
  hide?: boolean
  tagSeparator?: string
  separator?: string
  foreignKey?: CustomPropForeignKey
  definitions?: CustomPropDefinition[]
  maxLength?: number
  longtextOptions?: CustomPropLongTextoptions
  stepOptions?: CustomPropStepOptions
}
export type CustomPropOriginal = CustomProp & {
  name?: string
}
export type CustomProps = {
  fields: CustomProp[]
}
export type CustomPropsOriginal = {
  fields: CustomPropOriginal[]
}

// HEADER
export type TableHeader = {
  id: string
  name: string // mutualized for custom props and table schema
  tableSchema?: TableSchema
  customProps?: CustomProp
}
export type TableHeaderVirtual = TableHeader & {
  key: string
  keySlot: string
}

export enum TableAction {
  handleColumn = 'handleColumn',
  addColumn = 'addColumn',
  editColumn = 'editColumn',
  sortColumn = 'sortColumn',
  sortColumnByAsc = 'sortColumnByAsc',
  sortColumnByDesc = 'sortColumnByDesc',
  removeColumn = 'removeColumn',
  handleRow = 'handleRow',
  addRow = 'addRow',
  selectRow = 'selectRow',
  unselectRow = 'unselectRow',
  selectAllRows = 'selectAllRows',
  selectedRow = 'selectedRow',
  selectedRows = 'selectedRows',
  editRow = 'editRow',
  editCell = 'editCell',
  removeRow = 'removeRow',
  removeRows = 'removeRows'
}

export type TableHeaderHelper = TableHeader & {
  actions?: TableAction[]
}

// ---------------------
// META DATA
// ---------------------

export type Translation = {
  [locale: string]: string
}

export type MetaDataLogo = {
  src: string
  alt: string
}

export type MetaCredit = {
  title: Translation | string
  url?: string
  logo?: MetaDataLogo
}

export type MetaData = {
  title: Translation | string
  subtitle?: Translation | string
  description?: Translation | string
  open?: boolean
  canContribute?: boolean
  licence?: string
  author?: string
  collaborators?: string[]
  contact?: string
  filename?: string
  extension?: string
  credits?: MetaCredit[]
  logos?: MetaDataLogo[]
  tags?: string[]
  color?: string
  icon?: string
}

// ---------------------
// GIT INFOS
// ---------------------

export enum GitProvider {
  github = 'github',
  gitlab = 'gitlab'
}

export enum DatasetFileType {
  csv = 'csv',
  tsv = 'tsv',
  xls = 'xls',
  xlsx = 'xlsx',
  ods = 'ods'
}

export enum DatasetFileFamily {
  table = 'table',
  json = 'json',
  text = 'text'
}

export type DatasetGitInfo = {
  id: string
  uuid: string
  provider: GitProvider
  api: string
  orga?: string
  subgroups?: string
  subgroupsStr?: string
  repo: string
  branch: string | 'master' | 'main'
  repoUrl: string
  rawRoot: string
  fileraw: string
  publicRoot: string
  filepath: string
  filename: string
  filetype: DatasetFileType
  filefamily: DatasetFileFamily | 'other'
  filefullname: string
}

// ---------------------
// DATA ITEM
// ---------------------

export type TableItemData = {
  [headerId: string]: string | string[] | number | boolean | Date
}
export type TableItem = {
  id: string
  selectable?: boolean // Datami
  removable?: boolean // Datami
  data?: TableItemData
}

// ---------------------
// DATASET USER SETTINGS
// ---------------------

export enum ViewMode {
  preview = 'preview',
  edit = 'edit'
}

export enum SortByOrder {
  asc = 'asc',
  desc = 'desc',
  none = 'none'
}

export type HeaderUserSettingsSortBy = {
  headerId: string
  value: any
  order: SortByOrder
}

export type HeaderUserSettings = {
  [headerId: string]: {
    width?: number
    sortBy?: HeaderUserSettingsSortBy
  }
}

export type TableUserSettings = {
  viewMode?: ViewMode
  headers?: HeaderUserSettings
  pagination?: Pagination
}

// ---------------------
// DATASET
// ---------------------

export type DatasetBasicInfos = {
  id: string
  meta?: MetaData
  gitInfos?: DatasetGitInfo[]
  isSet?: boolean
}

export type Dataset = DatasetBasicInfos & {
  headers: TableHeader[] | TableHeaderHelper[]
  items: TableItem[]
  userSettings?: TableUserSettings
  schemaFile?: string
  customPropsFile?: string
}

// ---------------------
// DATASET <--> DB
// ---------------------
export type BodyDatasetNoId = {
  meta?: MetaData
  headers: TableHeader[]
  items: TableItem[]
  gitInfos?: DatasetGitInfo[]
  schemaFile?: string
  customPropsFile?: string
}

export type BodyCreateDataset = {
  userId: string
  data: BodyDatasetNoId
}

export type BodyGetDatasetById = {
  id: string
}

export type BodyGetDatasetsByUserId = {
  userId: string
}

export type BodyUpdateDataset = {
  userId: string
  data: BodyDatasetNoId
}

export type BodyDeleteDataset = {
  userId: string
  datasetId: string
}
