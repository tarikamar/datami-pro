export enum StepComponent {
  inputs = 'inputs',
  inputsFile = 'inputsFile',
  saveDataset = 'saveDataset',
  saveWidget = 'saveWidget'
}

export type StepItem = {
  code: string
  text: string
  icon?: string
  component?: StepComponent
  inputFields?: InputField[]
}
