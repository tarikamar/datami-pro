// export type PartialGithubProfile = {
//   githubLogin?: string
//   githubUrl?: string
//   githubRepos?: string
// }

// export type PartialGitlabProfile = {
//   gitlabLogin?: string
//   gitlabUrl?: string
//   gitlabRepos?: string
// }

// export type PartialGitProfile = PartialGithubProfile & PartialGitlabProfile

// export type BodyAccount = PartialGitProfile & {
//   userId: string
// }
export type BodyAccount = {
  userId: string
}
