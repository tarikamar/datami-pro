import type { WorkBook } from 'xlsx'
import { InputType, FileTarget } from '~/utils'

export type HandleImportFormattingOptions = {
  csvSeparator?: string
  csvSeparatorSuggested?: string
  xlsSheets?: object[]
  // xlsSheetsFirstRows?: object[]
  xlsSheetIndex?: number
  xlsHeadersIndex?: number
}

export type HandleImportFileEvent = {
  target: FileTarget
  inputType: InputType
  data: string | object
  workbook?: WorkBook
  fileName?: string
  fileExtension?: string
  options?: HandleImportFormattingOptions
}
