import type { Stripe } from 'stripe'
import type { UserDataBasic } from './userTypes'

export enum OfferFeatureCode {
  N_USERS = 'users',
  N_DATASETS = 'datasets',
  N_WIDGETS = 'widgets'
  // N_CONTRIBUTORS = 'contributors',
  // N_COLLABORATORS = 'collaborators'
}

export type OfferFeature = {
  title: string
  amount: number
  description?: string
  icon?: string
  featCode: OfferFeatureCode
  // datasets: number
  // contributors: number
  // collaborators: number
}

export const enum Currencies {
  EUR = 'eur',
  USD = 'usd',
  CAD = 'cad'
}
export type CurrencyBasic = {
  currency: Currencies
  isDefault?: boolean
}

export type StripePrices = {
  currency: Currencies
  default: number | undefined
  stripePriceKey?: string | number
  stripePrice?: number
}

export type BodyStripePrice = {
  productKey: string
}
export type BodyStripePrices = {
  productKeys: BodyStripePrice[]
}
export type StripeProductPrices = BodyStripePrice & {
  prices: Stripe.Price[]
}

export type BodyStripeProductPrices = {
  productKey: string
  priceKeys: string[]
}

export const enum SubscriptionCategory {
  FREE = 'FREE',
  PERSONAL = 'PERSONAL',
  PROFESSIONAL = 'PROFESSIONAL',
  PREMIUM = 'PREMIUM'
}

export type Offer = {
  title: string
  disable?: boolean
  subtitle: string
  description?: string
  // price: number
  features?: OfferFeature[]
  stripeProductKey?: string
  subscriptionCategory?: SubscriptionCategory
  prices?: StripePrices[]
}

export type StripePan = {
  id: string
  object: string
  currency: string
  interval: string
  amount: number
  nickname: string
  product: string
}

export type SubscriptionFromStripe = Stripe.Subscription & {
  quantity?: number
  amount?: number
  currency?: string
  plan?: StripePan
}

export type Subscription = {
  id?: string
  userId: string
  profileId: string
  organisationId?: string

  // STRIPE RELATED
  stripeId: string
  stripeStatus?: string
  stripeProductKey?: string
  stripeProductNickName?: string
  stripePriceId?: string
  quantity?: number
  amount?: number
  currency?: string
  trialEndsAt?: number | null
  endsAt?: number | null
  startDate?: number | null
  lastEventDate?: number | null

  // authorizations
  subscriptionCategory: SubscriptionCategory
  numberUsers?: number
  numberDatasets?: number
  numberWidgets?: number
}

export type SubPostRes = {
  url?: string
  return_url?: string
  clientSecret?: string
  user: UserDataBasic
  shouldUpdateUser: boolean
  error?: any
}

// FOR REQUESTS

export type BodySubscription = {
  userId: string
  lookupKey: string
}

// export const SubscriptionCategoryForPrisma: {
//   FREE: 'FREE'
//   PERSONAL: 'PERSONAL'
//   PROFESSIONAL: 'PROFESSIONAL'
//   PREMIUM: 'PREMIUM'
// } = {
//   FREE: 'FREE',
//   PERSONAL: 'PERSONAL',
//   PROFESSIONAL: 'PROFESSIONAL',
//   PREMIUM: 'PREMIUM'
// }
// export type SubscriptionCategoryForPrisma = (typeof SubscriptionCategoryForPrisma)[keyof typeof SubscriptionCategoryForPrisma]

// FOR STORE

export type UserSubscription = {
  stripeId?: string
  stripeProductKey?: string
  stripePriceId?: string

  // authorizations
  subscriptionCategory: SubscriptionCategory // | string
  numberUsers?: number
  numberDatasets?: number
  numberWidgets?: number

  endsAt?: number | null
  startDate?: number | null
}
