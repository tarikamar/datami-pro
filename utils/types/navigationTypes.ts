export enum ScrollDirection {
  vertical = 'verticalScroll',
  horizontal = 'horizontalScroll'
}
