export enum CsvSeparator {
  comma = ',',
  semicolon = ';',
  pipe = '|',
  tab = '\t'
}
export enum CsvSeparatorName {
  comma = 'comma',
  semicolon = 'semicolon',
  pipe = 'pipe',
  tab = 'tab'
}

export type CsvSeparatorObject = {
  separator: CsvSeparator
  name: CsvSeparatorName
}
