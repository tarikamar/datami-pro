export type UserStoreBasic = {
  userId: string
  name?: string
  email: string
  image?: string
}

export type UserDataBasic = {
  id: string
  email: string
  image?: string

  name?: string
  first_name: string
  last_name: string

  stripeCustomerId?: string
  subscriptions?: Subscription[]
}

export type UserDataClient = UserDataBasic & {
  email_verified: boolean

  role: 'SUPER_ADMIN' | 'ADMIN' | 'GENERAL'
  is_active: boolean
  // password: string
  csrf_token?: string
  current_password?: string
  new_password?: string
  last_login: Date | null
  created_at: Date
  deleted_at?: Date
  permissions: string | null
  avatar?: string | null
  updated_at?: Date
}

export type UserData = UserDataClient & {
  password: string
}

export type UserEditable = {
  first_name?: string
  last_name?: string
  role?: string
  csrf_token?: string
  is_active?: boolean
  permissions?: string
}

export type BodyUser = {
  userId: string
  first_name?: string
  last_name?: string
}

export type BodyUserStripe = {
  id: string
  stripeCustomerId: string
}

// LEGACY

export type NewUser = {
  name?: string
  first_name?: string
  last_name?: string
  email: string
  password: string
  csrf_token?: string
}

export type NewUserFromOauth = {
  name?: string
  email: string
  image?: string
  emailVerified?: Date
  password?: string
  uuid: string
}

export type UserLogin = {
  email: string
  password: string
  csrf_token?: string
}

export type UserInfos = {
  name?: string
  email: string
  image?: string
}

export enum Roles {
  'SUPER_ADMIN',
  'ADMIN',
  'GENERAL'
}
