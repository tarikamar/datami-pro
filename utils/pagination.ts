export const getClosest = (array: any[], goal: number) => {
  // https://stackoverflow.com/questions/8584902/get-the-closest-number-out-of-an-array
  return array.reduce((prev, curr) => {
    return Math.abs(curr - goal) < Math.abs(prev - goal) ? curr : prev
  })
}

export const paginate = (array: any[], pageSize: number, pageNumber: number) => {
  // human-readable page numbers usually start with 1, so we reduce 1 in the first argument
  return array.slice((pageNumber - 1) * pageSize, pageNumber * pageSize)
}
