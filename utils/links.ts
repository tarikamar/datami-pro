import { commonIcons } from '@/utils/icons'

export const homeLinks: BasicLink[] = [
  {
    text: 'pages.home',
    icon: commonIcons.home,
    to: '/'
  }
]

export const userDatasets: BasicLink[] = [
  {
    text: 'pages.datasets-overview',
    icon: commonIcons.datasets,
    to: '/datasets'
  },
  {
    text: 'datasets.create',
    icon: commonIcons.datasetsCreate,
    to: '/datasets/create'
  },
  {
    text: 'datasets.contributions',
    icon: commonIcons.datasetsContributions,
    to: '/datasets/contributions'
  }
]

export const userWidgets: BasicLink[] = [
  {
    text: 'widgets.overview',
    icon: commonIcons.widgets,
    to: '/widgets'
  },
  {
    text: 'widgets.create',
    icon: commonIcons.widgetsCreate,
    to: '/widgets/create'
  }
]

export const documentationLinks: BasicLink[] = [
  {
    text: 'documentation.overview',
    disable: true,
    icon: commonIcons.documentation,
    to: '/documentation/overview'
  },
  {
    text: 'documentation.examples',
    disable: true,
    icon: commonIcons.documentationExamples,
    to: '/documentation/examples'
  },
  {
    text: 'documentation.help',
    icon: commonIcons.documentationHelp,
    to: '/documentation/help'
  }
]

export const userLinks: BasicLink[] = [
  {
    text: 'user.settings.profile',
    icon: commonIcons.userProfile,
    to: '/me/profile'
  },
  {
    text: 'user.settings.settings',
    disable: true,
    icon: commonIcons.userSettings,
    to: '/me/settings'
  },
  // {
  //   text: 'user.settings.myOrganisations',
  //   icon: commonIcons.userOrganisations,
  //   to: '/me/organisations'
  // },
  {
    text: 'user.settings.subscription',
    icon: commonIcons.userSubscription,
    to: '/me/subscription'
  }
]
