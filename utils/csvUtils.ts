import { CsvSeparator, type CsvSeparatorObject, CsvSeparatorName } from '@/utils'

export const separators: CsvSeparatorObject[] = [
  { separator: CsvSeparator.comma, name: CsvSeparatorName.comma },
  { separator: CsvSeparator.semicolon, name: CsvSeparatorName.semicolon },
  { separator: CsvSeparator.pipe, name: CsvSeparatorName.pipe },
  { separator: CsvSeparator.tab, name: CsvSeparatorName.tab }
]

interface CsvParserOptions {
  separator?: string | CsvSeparator
  tagseparator?: string
  asJson?: boolean
  quoteChar?: string
  schema?: any
}

/**
 * @typedef {Object} options
 * @property {string} separator
 * @property {string} tagseparator
 * @property {boolean} asJson
 **/

export const defaultCsvOptions: CsvParserOptions = {
  separator: CsvSeparator.semicolon,
  tagseparator: '-',
  asJson: false,
  quoteChar: undefined,
  schema: undefined
}

export const dblQuotesDatami = '~~datami-quotes~~'
export const breaklineDatami = '~~datami-br~~'

// cf : https://stackoverflow.com/questions/28543821/convert-csv-lines-into-javascript-objects

/**
 * parseLine takes a string parses it as a lien object
 * @param  {string} line string equivalent to a line content encoded as JSON
 * @return {object}      line as an object
 **/
export const parseLine = (line: string) => JSON.parse(`[${line}]`)

/**
 * @typedef {Object} csv
 * @property {Object} headers The csv headers
 * @property {Object} data The csv data
 **/

// cf : https://stackoverflow.com/questions/59218548/what-is-the-best-way-to-convert-from-csv-to-json-when-commas-and-quotations-may
/**
 * Takes a raw CSV string and converts it to a JavaScript object.
 * @param {string} text The raw CSV string.
 * @param {CsvSeparator} separator A character to use between columns.
 * @param {string} quoteChar A character to use as the encapsulating character.
 * @param {string[]} headers An optional array of headers to use. If none are
 * given, they are pulled from the first line of `text`.
 * @returns {object[]} An array of JavaScript objects containing headers as keys
 * and row entries as values.
 */
export const csvToJson = (
  text: string,
  separator: CsvSeparator | string = CsvSeparator.comma,
  quoteChar = '"',
  headers = undefined,
  schema: any | undefined = undefined
) => {
  // console.log('\nU > csvToJson > headers : ', headers)
  // console.log('U > csvToJson > schema : ', schema)
  // console.log('U > csvToJson > quoteChar : ', quoteChar)
  // console.log('U > csvToJson > text : ', text)

  const dblQuoteChar = quoteChar.repeat(2) // `${quoteChar}${quoteChar}`

  // replace dblQuotes
  let textClean: string | string[] = text.replaceAll(dblQuoteChar, dblQuotesDatami)
  // console.log('U > csvToJson > textClean (start) : ', textClean)

  // prepare regex
  const quotesRegex = new RegExp(`(${quoteChar}.*?${quoteChar})`)
  const regex = new RegExp(`\\s*(${quoteChar})?(.*?)\\1\\s*(?:${separator}|$)`, 'gs')
  // console.log('\nU > csvToJson > quotesRegex : ', quotesRegex)

  // textClean with single quoteChar without linebreaks
  textClean = textClean.split(quotesRegex)
  // console.log('\nU > csvToJson > textClean (A) : ', textClean)
  textClean = textClean.map((item) => {
    const s = item.includes(quoteChar) ? item.replace('\n', breaklineDatami) : item
    return s
  })
  // console.log('\nU > csvToJson > textClean (B) : ', textClean)
  textClean = textClean.join('')
  // console.log('\nU > csvToJson > textClean (C) : ', textClean)

  const match = (line: string) => {
    const matches = [...line.matchAll(regex)].map((m) => m[2])
    matches.pop() // cut off blank match at the end
    return matches
  }

  let lines: string[] = textClean.split('\n')
  // filter empty lines
  lines = lines.filter((l) => l !== '')

  // get csv headers
  const heads = headers ?? match(lines.shift() as string)
  // console.log('U > csvToJson > lines : ', lines)
  // console.log('U > csvToJson > heads : ', heads)
  const headsEnriched = heads.map((h) => {
    const headerFromSchema = schema && schema.fields && schema.fields.find((f: any) => f.name === h)
    const header = headerFromSchema || { name: h }
    return header
  })
  // console.log('U > csvToJson > headsEnriched : ', headsEnriched)

  return lines.map((line) => {
    return match(line).reduce((acc, cur, i) => {
      //  get corresponding header
      const header = headsEnriched[i]
      // for debugging
      // console.log('U > csvToJson > header : ', header)
      // const needDebug = header.name.includes(breaklineDatami)
      // needDebug && console.log('U > csvToJson > header : ', header)
      const cellType = (header && header.type) || 'string'
      // console.log('U > csvToJson > cellType : ', cellType)

      // get value according to schema
      // let val = cur.length === 0 ? null : cur
      let val: number | string = cur.replaceAll(dblQuotesDatami, quoteChar).replaceAll(breaklineDatami, '\n') || cur
      if (cellType === 'number') {
        // Attempt to parse as a number
        val = Number(cur) || val
      } else if (cellType === 'integer') {
        // Attempt to parse as an integer
        val = parseInt(cur) || val
      }
      const key = (header && header.name) || `extra_datami_header_${i}`
      return { ...acc, [key]: val }
    }, {})
  })
}

/**
 * Takes an array of objects and a dict of keys
 * @param {object[]} arr An array of objects as { header_label : value }
 * @param {object} headers The headers object as { header_index : header_label }
 * @returns {object[]} An array of JavaScript objects containing headers as header_index
 * and row entries as values, as { header_index : value }.
 */
export const changeKeyObjects = (arr: object[], headers: object) => {
  // console.log('\nU > changeKeyObjects > arr : ', arr)
  // console.log('U > changeKeyObjects > headers : ', headers)
  return arr.map((item) => {
    const newItem = {}
    Object.keys(headers).forEach((key) => {
      newItem[key] = item[headers[key]]
    })
    return newItem
  })
}

/**
 * csvToObject takes a string parses it as a lien object
 * @param  {string} csvRaw string equivalent to csv content
 * @param  {options} options object containing the options to parse csv content
 * @return {csv} csv as an object
 **/
export const csvToObject = (csvRaw: string, options: CsvParserOptions = defaultCsvOptions) => {
  // console.log('\nU > csvToObject > options : ', options)
  // console.log('U > csvToObject > csvRaw : \n', csvRaw)

  const separator = options.separator || CsvSeparator.comma
  const quoteChar = options.quoteChar || '"'
  let headersArr

  // split data into lines
  const headerLine = csvRaw.split('\n')[0].trim()
  // console.log('U > csvToObject > headerLine : ', headerLine)

  // use csvToJson function
  let lines = csvToJson(csvRaw, separator, quoteChar, headersArr, options.schema)
  // console.log('U > csvToObject > lines (A) : ', lines)

  // get headers
  if (options.asJson) {
    headersArr = parseLine(headerLine)
  } else {
    headersArr = headerLine.split(separator)
  }
  // console.log('U > csvToObject > headersArr : ', headersArr)

  const headers = { ...headersArr }
  // console.log('U > csvToObject > headers : ', headers)

  lines = changeKeyObjects(lines, headers)
  // console.log('U > csvToObject > lines (B) : ', lines)

  // add id to each line
  const objects = lines.map((line, index) => {
    const lineWithId = {
      ...line,
      ...{ id: index.toString() }
    }
    return lineWithId
  })
  // console.log('U > csvToObject > headers : ', headers)
  // console.log('U > csvToObject > objects : ', objects)

  // return csv object
  const csv = {
    headers,
    data: objects
  }

  return csv
}

interface HeaderCsv {
  label: string
  field: string
}

/**
 * ObjectToCsv takes headers infos and data to build a string corresponding to a raw csv
 * @param  {headers} headers Array of headers
 * @param  {data} data Array of objects
 * @param  {options} options object containing the options to parse csv content
 * @return {csv} csv as an object
 **/
export const ObjectToCsv = (headers: HeaderCsv[], data: object[], options = defaultCsvOptions, quoteChar = '"') => {
  // console.log('\nU > ObjectToCsv > headers : \n', headers)
  // console.log('U > ObjectToCsv > data : \n', data)
  // console.log('U > ObjectToCsv > options : ', options)

  const dblQuoteChar = quoteChar.repeat(2) // `${quoteChar}${quoteChar}`
  let csvOut = ''

  // build headers - 1st line
  const headersLabels = headers.map((h: HeaderCsv) => h.label).join(options.separator)
  csvOut += `${headersLabels}\n`
  // console.log('U > ObjectToCsv > csvOut : ', csvOut)

  // build rows
  data.forEach((d) => {
    // console.log('U > ObjectToCsv > d : ', d)
    const dataStr = headers
      .map((h: HeaderCsv) => {
        let val = d[h.field]
        const hasSep = val && val.toString().includes(options.separator)
        const hasQuotesIn = val && val.toString().includes(quoteChar)
        // const hasBreaklineIn = val && val.toString().includes('\n')
        const addQuotesExt = hasSep ? quoteChar : ''
        val = hasQuotesIn ? val.replaceAll(quoteChar, dblQuoteChar) : val
        const valStr = `${addQuotesExt}${val}${addQuotesExt}`
        return valStr
      })
      .join(options.separator)
    csvOut += `${dataStr}\n`
  })

  return csvOut
}
