export const licences = [
  // { key: 'MIT Licence', value: 'MIT' }, //
  // { key: 'AGPL Licence', value: 'GNU AGPL' }

  // Identical
  {
    key: 'ODC Open Database License (ODbL) version 1.0', //
    value: 'ODbL-1.0',
    reciprocity: false,
    identical: true
  },

  // Permissive
  {
    key: 'Apache License 2.0', //
    value: 'Apache-2.0',
    reciprocity: false,
    identical: false
  },
  {
    key: 'BSD 2-Clause "Simplified" License', //
    value: 'BSD-2-Clause',
    reciprocity: false,
    identical: false
  },
  {
    key: 'BSD 3-Clause "New" or "Revised" License', //
    value: 'BSD-3-Clause',
    reciprocity: false,
    identical: false
  },
  {
    key: 'CeCILL-B Free Software License Agreement', //
    value: 'CECILL-B',
    reciprocity: false,
    identical: false
  },
  {
    key: 'MIT License', //
    value: 'MIT',
    reciprocity: false,
    identical: false
  },

  // Reciprocity
  {
    key: 'CeCILL Free Software License Agreement v2.1', //
    value: 'CECILL-2.1',
    reciprocity: true,
    identical: false
  },
  {
    key: 'CeCILL-C Free Software License Agreement', //
    value: 'CECILL-C',
    reciprocity: true,
    identical: false
  },
  {
    key: 'GNU General Public License v3.0 or later', //
    value: 'GPL-3.0-or-later',
    reciprocity: true,
    identical: false
  },
  {
    key: 'GNU Lesser General Public License v3.0 or later', //
    value: 'LGPL-3.0-or-later',
    reciprocity: true,
    identical: false
  },
  {
    key: 'GNU Affero General Public License v3.0 or later', //
    value: 'AGPL-3.0-or-later',
    reciprocity: true,
    identical: false
  },
  {
    key: 'Mozilla Public License 2.0', //
    value: 'MPL-2.0',
    reciprocity: true,
    identical: false
  },
  {
    key: 'Eclipse Public License 2.0', //
    value: 'EPL-2.0',
    reciprocity: true,
    identical: false
  },
  {
    key: 'European Union Public License 1.2', //
    value: 'EUPL-1.2',
    reciprocity: true,
    identical: false
  }
]
