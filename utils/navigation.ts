import { ScrollDirection } from '@/utils'

export const scrollToLastElement = (containerId: string, direction: ScrollDirection) => {
  // const options: ScrollIntoViewOptions = {
  //   behavior: 'smooth',
  //   block: direction === ScrollDirection.vertical ? 'start' : 'nearest',
  //   inline: direction === ScrollDirection.vertical ? 'nearest' : 'center'
  // }
  // console.log('scrollToElement > containerId :', containerId)

  const container = document.querySelector(containerId) as HTMLElement
  // console.log('scrollToElement > container :', container)

  const scrollable = container.querySelector('.v-table__wrapper') as HTMLElement
  // console.log('scrollToElement > scrollable :', scrollable)

  if (direction === ScrollDirection.vertical) {
    // console.log('scrollToElement > scrollHeight :', scrollable.scrollHeight)
    scrollable.scrollTop += scrollable.scrollHeight
  } else {
    // console.log('scrollToElement > scrollWidth :', scrollable.scrollWidth)
    scrollable.scrollLeft += scrollable.scrollWidth
  }
}
