// import { Variant } from '@/utils'
import { licences } from '@/utils/licences'

// export const datasetTitleOptions = [
//   { table: InputTable.datasets, key: 'meta.title', inputType: InputType.text } //
// ]
const sharedTable = InputTable.datasets
// const sharedInputLocal = LocalItem.meta
// const sharedVariant = Variant.outlined

// -------------------------
// META INFOS
// -------------------------

export const datasetMetaBasics: InputField[] = [
  {
    table: sharedTable, //
    key: 'title',
    inputType: InputType.text,
    inputLocal: LocalItem.meta
    // variant: sharedVariant
  },
  {
    table: sharedTable,
    key: 'subtitle',
    inputType: InputType.text,
    inputLocal: LocalItem.meta
    // variant: sharedVariant
  },
  {
    table: sharedTable,
    key: 'description',
    inputType: InputType.textarea,
    inputLocal: LocalItem.meta
    // variant: sharedVariant
  }
]

export const datasetMetaHistory: InputField[] = [
  {
    table: sharedTable,
    key: 'author',
    inputType: InputType.text,
    inputLocal: LocalItem.meta,
    readonly: true
    // variant: sharedVariant
  },
  {
    table: sharedTable,
    key: 'collaborators',
    inputType: InputType.combobox,
    inputLocal: LocalItem.meta,
    allowNew: true
    // variant: sharedVariant
  }
]
export const datasetMetaContacts: InputField[] = [
  {
    table: sharedTable,
    key: 'contact',
    inputType: InputType.email,
    inputLocal: LocalItem.meta,
    readonly: true
    // variant: sharedVariant
  }
]

export const datasetMetaExtras: InputField[] = [
  {
    table: sharedTable,
    key: 'tags',
    inputType: InputType.combobox,
    inputLocal: LocalItem.meta,
    allowNew: true
    // variant: sharedVariant
    // options: {
    //   select: [{key: '1', value: 1}],
    //   selectKey: 'key',
    //   selectValue: 'value'
    // }
  },
  {
    table: sharedTable,
    key: 'icon',
    inputType: InputType.select,
    inputLocal: LocalItem.meta,
    // variant: sharedVariant
    options: {
      select: datasetIcons,
      asIcon: true
      // selectKey: 'key',
      // selectValue: 'value'
    }
  },
  {
    table: sharedTable,
    key: 'color',
    inputType: InputType.color,
    inputLocal: LocalItem.meta
    // variant: sharedVariant
  }
]

export const datasetMetaLegal: InputField[] = [
  {
    table: sharedTable,
    key: 'licence',
    inputType: InputType.select,
    inputLocal: LocalItem.meta,
    allowNew: true,
    options: {
      select: licences,
      selectKey: 'key',
      selectValue: 'value'
    }
    // variant: sharedVariant
  }
]

export const datasetMetaAuths: InputField[] = [
  {
    table: sharedTable,
    key: 'open',
    inputType: InputType.switch,
    inputLocal: LocalItem.meta
    // variant: sharedVariant
  },
  {
    table: sharedTable,
    key: 'canContribute',
    inputType: InputType.switch,
    inputLocal: LocalItem.meta
    // variant: sharedVariant
  }
]

export const datasetMetaFile: InputField[] = [
  {
    table: sharedTable,
    key: 'filename',
    inputType: InputType.text,
    inputLocal: LocalItem.meta,
    readonly: true
    // variant: sharedVariant
  },
  {
    table: sharedTable,
    key: 'extension',
    inputType: InputType.text,
    inputLocal: LocalItem.meta,
    readonly: true
    // variant: sharedVariant
  },
  {
    table: sharedTable,
    key: 'createdAt',
    inputType: InputType.text,
    inputLocal: LocalItem.meta,
    readonly: true
    // variant: sharedVariant
  }
]
