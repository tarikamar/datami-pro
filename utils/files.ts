// Custom functions for files uploading

import { v4 as uuidv4 } from 'uuid'
import { read, utils, type WorkBook } from 'xlsx'
import {
  InputType, //
  FileTarget,
  type HandleImportFileEvent,
  type TableHeader,
  type TableSchemaOfficial,
  type TableSchemaOfficialWrapper,
  type CustomProps,
  type FlatObject
} from '~/utils'
import { separators } from '@/utils/csvUtils'

// CONSTANTS

export const createOptions = [
  { table: InputTable.datasets, key: 'file', inputType: InputType.fileBlank } //
]

export const importFileOptions = [
  { table: InputTable.datasets, key: 'file', inputType: InputType.fileCsv },
  { table: InputTable.datasets, key: 'file', inputType: InputType.fileXls }
]
export const importSchemaOptions = [
  { table: InputTable.datasets, key: 'file', inputType: InputType.fileJson } //
]
export const importDistOptions = [
  { table: InputTable.datasets, key: 'file', inputType: InputType.fileUrl } //
]

export const accetpsFormats = (inputType: InputType) => {
  switch (inputType) {
    case InputType.fileCsv:
      return '.csv'
    case InputType.fileXls:
      return 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    case InputType.fileJson:
      return 'application/json'
    case InputType.fileGeoJson:
      return 'application/json'
    case InputType.fileMarkdown:
      return 'text/plain'
    case InputType.fileText:
      return 'text/plain'
    case InputType.fileImage:
      return 'image/*'
    default:
      return ''
  }
}

// ------------
// PREVIEWS
// ------------

// For CSV files
export const suggestCsvFileSeparator = (csvRaw: string) => {
  // const separatorCounts = [...separators]
  const separatorCounts = separators.map((sep) => {
    const sepCount = csvRaw.split(sep.separator).length
    return { ...sep, count: sepCount }
  })
  const countValues = separatorCounts.map((sepCount) => sepCount.count)
  const indexOfGreatestValue = countValues.indexOf(Math.max(...countValues))
  return {
    counts: separatorCounts,
    suggested: separatorCounts[indexOfGreatestValue]
  }
}

// For XLS files
export const getXlsSheetNames = (workbook: WorkBook) => {
  return workbook.SheetNames
}

export const getXlsSheetFirstRows = (workbook: WorkBook, sheetName: string, max: number = 9) => {
  let rows = workbook.Sheets[sheetName]
  rows = utils.sheet_to_json(rows).slice(0, max)
  return { sheetName, rows }
}

export const getFileImportOptions = ({
  inputType, //
  data
  // fileName,
  // fileExtension
}: HandleImportFileEvent) => {
  let csvSeparatorSuggested, //
    workbook,
    xlsSheets
  // xlsSheetsFirstRows

  // CSV CASE
  if (inputType === InputType.fileCsv) {
    //
    csvSeparatorSuggested = suggestCsvFileSeparator(data as string)
  }

  // EXCEL CASE
  if (inputType === InputType.fileXls) {
    const workbook: WorkBook = read(data)
    console.log('formatFileData > workbook : ', workbook)
    xlsSheets = getXlsSheetNames(workbook).map((name, index) => {
      return { name, index }
    })
    // xlsSheetsFirstRows = xlsSheets.map((sheet) => {
    //   let rows = getXlsSheetFirstRows(workbook, sheet.name)
    //   console.log('formatFileData > rows : ', rows)
    //   rows = utils.sheet_to_json(rows).slice(0, 9)
    //   return { sheetName: sheet.name, sheetIndex: sheet.index, rows }
    // })
  }

  return {
    workbook,
    options: {
      csvSeparatorSuggested,
      xlsSheets
      // xlsSheetsFirstRows
    },
    preview: {}
  }
}

// ------------
// FORMATTERS
// ------------

export const formatFileData = ({ inputType, data, fileName, fileExtension, options }: HandleImportFileEvent, headers?: TableHeader[]) => {
  // console.log('formatFileData > inputType : ', inputType)
  // console.log('formatFileData > headers : ', headers)
  // console.log('formatFileData > data : ', data)
  // console.log('formatFileData > fileName : ', fileName)
  // console.log('formatFileData > fileExtension : ', fileExtension)

  let headersStructured: any, dataStructured: any
  let formmattedData = data

  // Dataset unique identifier
  const uuid = uuidv4()

  // JSON case
  if (inputType === InputType.fileJson || inputType === InputType.fileGeoJson) {
    formmattedData = data && JSON.parse(data as string)
  }

  // CSV CASE
  if (inputType === InputType.fileCsv) {
    const csvObj: any = (data as string) && csvToObject(data as string, { separator: options?.csvSeparator || ',' })
    headersStructured = Object.keys(csvObj.headers).map((i: string) => {
      return {
        id: parseInt(i).toString(),
        name: csvObj.headers[i],
        // customProps : [],
        tableSchema: {
          type: 'string', // TO DO - change depending on existing header or not
          title: csvObj.headers[i]
        }
      }
    })
    dataStructured = csvObj.data.map((d: object, i: number) => {
      return {
        id: `item-${i}`,
        data: d
      }
    })
    formmattedData = {
      id: uuid,
      meta: {
        title: fileName,
        filename: fileName,
        extension: fileExtension
      } as MetaData,
      headers: headersStructured as TableHeader[],
      items: dataStructured as TableItem[]
    }
  }

  // EXCEL CASE
  if (inputType === InputType.fileXls) {
    const workbook: WorkBook = read(data)
    // console.log('formatFileData > workbook : ', workbook)
    const worksheet = workbook.Sheets[workbook.SheetNames[options?.xlsSheetIndex || 0]]
    const table = utils.sheet_to_json(worksheet, { header: options?.xlsHeadersIndex || 'A' })
    // console.log('formatFileData > table : ', table)

    const headersRow: FlatObject = table[0] as FlatObject
    headersStructured = Object.keys(headersRow).map((i: string) => {
      return {
        id: i,
        name: headersRow[i],
        // customProps : [],
        tableSchema: {
          type: 'string', // TO DO - change depending on existing header or not
          title: headersRow[i]
        }
      }
    })

    const dataRows = table.slice(1, table.length - 1)
    // console.log('formatFileData > dataRows : ', dataRows)
    dataStructured = dataRows.map((d, i) => {
      return {
        id: `item-${i}`,
        data: d
      }
    })

    formmattedData = {
      id: uuid,
      meta: {
        title: fileName,
        filename: fileName,
        extension: fileExtension
      } as MetaData,
      headers: headersStructured,
      items: dataStructured
    }
  }

  // console.log('formatFileData > formmattedData : ', formmattedData)
  return formmattedData
}

// MIXERS

export const mixJsonWithDataset = ({ target, json, data }: { target?: FileTarget; json: any; data: any }) => {
  // console.log('mixJsonWithDataset > target : ', target)
  // console.log('mixJsonWithDataset > json : ', json)
  // console.log('mixJsonWithDataset > data : ', data)

  let updated: any

  // Mix existing data w/ imported json
  switch (target) {
    // JSON is a table schema file
    case FileTarget.tableSchema:
      updated = toRaw(data) as TableHeader[]
      const schema = toRaw(json) as TableSchemaOfficialWrapper
      schema.fields?.forEach((fieldRaw: TableSchemaOfficial) => {
        // console.log('\nmixJsonWithDataset > fieldRaw : ', fieldRaw)
        const existingHeaderIndex = updated.findIndex((h: TableHeader) => h.name === fieldRaw.name)
        // console.log('mixJsonWithDataset > existingHeaderIndex : ', existingHeaderIndex)
        if (existingHeaderIndex !== -1) {
          const existingHeader = updated[existingHeaderIndex]
          // console.log('mixJsonWithDataset > existingHeader A : ', existingHeader)
          delete fieldRaw.name
          existingHeader.tableSchema = { ...existingHeader.tableSchema, ...fieldRaw }
          // console.log('mixJsonWithDataset > existingHeader B : ', existingHeader)
          updated[existingHeaderIndex] = existingHeader
        }
      })
      break

    // JSON is a custom props file
    case FileTarget.customProps:
      updated = toRaw(data) as TableHeader[]
      const customProps = toRaw(json) as CustomPropsOriginal
      customProps.fields?.forEach((fieldRaw: CustomPropOriginal) => {
        // console.log('\nmixJsonWithDataset > fieldRaw : ', fieldRaw)
        const existingHeaderIndex = updated.findIndex((h: TableHeader) => h.name === fieldRaw.name)
        // console.log('mixJsonWithDataset > existingHeaderIndex : ', existingHeaderIndex)
        if (existingHeaderIndex !== -1) {
          const existingHeader = updated[existingHeaderIndex]
          // console.log('mixJsonWithDataset > existingHeader A : ', existingHeader)
          delete fieldRaw.name
          existingHeader.customProps = { ...existingHeader.customProps, ...fieldRaw }
          // console.log('mixJsonWithDataset > existingHeader B : ', existingHeader)
          updated[existingHeaderIndex] = existingHeader
        }
      })
      break
  }

  return updated
}
