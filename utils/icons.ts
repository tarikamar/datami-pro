import { InputType, TableAction, TableHeaderType, TableHeaderSubtype } from '@/utils'

export const commonIcons = {
  datasets: 'mdi-database',
  datasetsCreate: 'mdi-database-plus',
  datasetsContributions: 'mdi-message-text',
  datasetHeaderSettings: 'mdi-table-cog',

  widgets: 'mdi-application-import',
  widgetsCreate: 'mdi-application-brackets-outline',

  documentation: 'mdi-text-box-multiple',
  documentationExamples: 'mdi-eye',
  documentationHelp: 'mdi-help',

  userProfile: 'mdi-account-outline',
  userSettings: 'mdi-cog',
  userOrganisations: 'mdi-office-building-outline',
  userSubscription: 'mdi-credit-card-check-outline',

  home: 'mdi-home',
  subscriptions: 'mdi-credit-card-outline',

  login: 'mdi-login',
  account: 'mdi-account-outline',
  users: 'mdi-account-multiple',
  register: 'mdi-account-plus-outline',
  email: 'mdi-email-outline',

  save: 'mdi-content-save-outline',
  download: 'mdi-download',
  refresh: 'mdi-refresh',
  reload: 'mdi-refresh',
  edit: 'mdi-pencil-outline',
  editFill: 'mdi-pencil',
  delete: 'mdi-trash-can',
  remove: 'mdi-trash-can-outline',
  lock: 'mdi-lock-outline',
  unlock: 'mdi-lock-open-variant-outline',
  close: 'mdi-close',
  add: 'mdi-plus-thick',
  validate: 'mdi-check-bold',
  handleVertical: 'mdi-dots-vertical',
  handleHorizontal: 'mdi-dots-horizontal',

  select: 'mdi-checkbox-blank-outline',
  selected: 'mdi-checkbox-marked',
  allSelected: 'mdi-checkbox-multiple-outline',
  someSelected: 'mdi-minus-box-outline',
  selectMultiple: 'mdi-checkbox-multiple-blank-outline',

  day: 'mdi-white-balance-sunny',
  night: 'mdi-moon-waning-crescent',

  right: 'mdi-chevron-right',
  left: 'mdi-chevron-left',
  up: 'mdi-chevron-up',
  down: 'mdi-chevron-down',

  sort: 'mdi-sort',
  sortAsc: 'mdi-sort-ascending',
  sortDesc: 'mdi-sort-descending',

  table: 'mdi-table',
  json: 'mdi-code-json',
  customProps: 'mdi-file-link-outline',

  preview: 'mdi-eye',
  search: 'mdi-magnify',

  info: 'mdi-information-slab-circle-outline',
  infoActive: 'mdi-information-slab-circle',
  warning: 'mdi-alert',

  git: 'mdi-git',
  github: 'mdi-github',
  gitlab: 'mdi-gitlab',
  openData: 'mdi-eye-outline',
  closeData: 'mdi-eye-off-outline'
}

export const findInputIcon = (inputType: string | undefined) => {
  switch (inputType) {
    case InputType.file:
      return 'mdi-file-outline'
    case InputType.fileBlank:
      return 'mdi-file-plus-outline'
    case InputType.fileCsv:
      return 'mdi-file-delimited-outline'
    case InputType.fileXls:
      return 'mdi-file-table-box-multiple'
    case InputType.fileJson:
      return 'mdi-code-json'
    case InputType.fileGeoJson:
      return 'mdi-code-json'
    case InputType.fileMarkdown:
      return 'mdi-language-markdown-outline'
    case InputType.fileImage:
      return 'mdi-file-image-outline'
    case InputType.fileUrl:
      return 'mdi-web'
    case InputType.text:
      return 'mdi-format-text'
    case InputType.email:
      return 'mdi-at'
    case InputType.password:
      return 'mdi-lock-outline'
    case InputType.textarea:
      return 'mdi-text-long'
    case InputType.checkbox:
      return 'mdi-check'
    case InputType.number:
      return 'mdi-numeric'
    case InputType.range:
      return 'mdi-counter'
    case InputType.switch:
      return 'mdi-toggle-switch'
    case InputType.select:
      return 'mdi-check-underline'
    case InputType.combobox:
      return 'mdi-playlist-check'
    case InputType.radio:
      return 'mdi-radiobox'
    case InputType.object:
      return 'mdi-code-json'
    case InputType.array:
      return 'mdi-code-json'
    default:
      return 'mdi-information-box-outline'
  }
}

export const findActionIcon = (action: TableAction) => {
  switch (action) {
    case TableAction.handleRow:
      return commonIcons.handleHorizontal
    case TableAction.addRow:
      return commonIcons.add
    case TableAction.selectRow:
      return commonIcons.select
    case TableAction.selectAllRows:
      return commonIcons.select
    case TableAction.selectedRow:
      return commonIcons.selected
    case TableAction.selectedRows:
      return commonIcons.selected
    case TableAction.editRow:
      return commonIcons.edit
    case TableAction.editCell:
      return commonIcons.edit
    case TableAction.removeRow:
      return commonIcons.remove
    case TableAction.removeRows:
      return commonIcons.remove
    case TableAction.handleColumn:
      return commonIcons.handleVertical
    case TableAction.addColumn:
      return commonIcons.add
    case TableAction.editColumn:
      return commonIcons.editFill
    case TableAction.sortColumn:
      return commonIcons.sort
    case TableAction.sortColumnByAsc:
      return commonIcons.sortAsc
    case TableAction.sortColumnByDesc:
      return commonIcons.sortDesc
    case TableAction.removeColumn:
      return commonIcons.remove
    default:
      return commonIcons.handleVertical
  }
}

export const findHeaderTypeIcon = (header: TableHeader) => {
  switch (header.tableSchema?.type) {
    case TableHeaderType.string:
      switch (header.customProps?.subtype) {
        case TableHeaderSubtype.tag:
          return 'mdi-tag-outline'
        case TableHeaderSubtype.tags:
          return 'mdi-tag-multiple-outline'
        case TableHeaderSubtype.longtext:
          return 'mdi-text-long'
        case TableHeaderSubtype.link:
          return 'mdi-link-variant'
        case TableHeaderSubtype.links:
          return 'mdi-link-variant'
        case TableHeaderSubtype.email:
          return 'mdi-at'
        case TableHeaderSubtype.emails:
          return 'mdi-at'
        case TableHeaderSubtype.image:
          return 'mdi-image-outline'
        case TableHeaderSubtype.timelinetext:
          return 'mdi-timeline'
        default:
          return 'mdi-format-text'
      }

    case TableHeaderType.integer:
      return 'mdi-numeric'

    case TableHeaderType.float:
      return 'mdi-numeric'

    case TableHeaderType.number:
      switch (header.customProps?.subtype) {
        case TableHeaderSubtype.percent:
          return 'mdi-percent-outline'
        case TableHeaderSubtype.geopoint:
          return 'mdi-map-marker'
        default:
          return 'mdi-numeric'
      }

    case TableHeaderType.boolean:
      return 'mdi-check'

    case TableHeaderType.date:
      return 'mdi-calendar-range'

    case TableHeaderType.any:
      return 'mdi-format-text'

    default:
      return 'mdi-format-text'
  }
}

export const datasetIcons = [
  // geometries
  'mdi-apps',
  'mdi-view-agenda',
  'mdi-view-agenda-outline',
  'mdi-view-carousel',
  'mdi-view-carousel-outline',
  'mdi-view-column',
  'mdi-view-column-outline',
  'mdi-view-comfy',
  'mdi-view-comfy-outline',
  'mdi-view-compact',
  'mdi-view-compact-outline',
  'mdi-view-dashboard',
  'mdi-view-dashboard-outline',
  'mdi-view-gallery',
  'mdi-view-gallery-outline',
  'mdi-view-grid',
  'mdi-view-grid-outline',
  'mdi-view-list',
  'mdi-view-list-outline',
  'mdi-view-module',
  'mdi-view-module-outline',
  'mdi-view-sequential',
  'mdi-view-sequential-outline',
  'mdi-cube',
  'mdi-cube-outline',
  'mdi-cone',
  'mdi-cylinder',
  'mdi-pyramid',
  'mdi-sphere',
  'mdi-heart',
  'mdi-heart-outline',
  'mdi-star',
  'mdi-star-outline',
  'mdi-star-circle',
  'mdi-star-circle-outline',
  'mdi-rhombus-split',
  'mdi-rhombus-split-outline',
  'mdi-vector-combine',
  'mdi-vector-link',
  'mdi-hexagon-multiple-outline',
  // data / dataviz / files
  'mdi-chart-donut',
  'mdi-flash',
  'mdi-flash-outline',
  'mdi-lightning-bolt',
  'mdi-lightning-bolt-outline',
  'mdi-snowflake',
  'mdi-umbrella',
  'mdi-umbrella-outline',
  'mdi-database',
  'mdi-database-outline',
  'mdi-file',
  'mdi-file-outline',
  'mdi-file-cabinet',
  'mdi-file-chart',
  'mdi-file-chart-outline',
  'mdi-file-code',
  'mdi-file-code-outline',
  'mdi-file-delimited',
  'mdi-file-delimited-outline',
  'mdi-file-document',
  'mdi-file-document-outline',
  'mdi-file-excel',
  'mdi-file-excel-outline',
  'mdi-file-excel-box',
  'mdi-file-excel-box-outline',
  'mdi-file-image',
  'mdi-file-image-outline',
  'mdi-file-table',
  'mdi-file-table-outline',
  'mdi-file-table-box',
  'mdi-file-table-box-outline',
  'mdi-file-table-box-multiple',
  'mdi-file-table-box-multiple-outline',
  'mdi-file-star',
  'mdi-folder',
  'mdi-folder-outline',
  'mdi-folder-multiple',
  'mdi-folder-multiple-outline',
  'mdi-folder-table',
  'mdi-folder-table-outline',
  'mdi-text-box',
  'mdi-text-box-outline',
  'mdi-text-box-multiple',
  'mdi-text-box-multiple-outline',
  // animals
  'mdi-bat',
  'mdi-bee',
  'mdi-bone',
  'mdi-bird',
  'mdi-bug-outline',
  'mdi-bug',
  'mdi-butterfly',
  'mdi-cat',
  'mdi-cow',
  'mdi-dog',
  'mdi-dog-side',
  'mdi-dolphin',
  'mdi-donkey',
  'mdi-duck',
  'mdi-elephant',
  'mdi-horse',
  'mdi-jellyfish',
  'mdi-kangaroo',
  'mdi-koala',
  'mdi-linux',
  'mdi-paw',
  'mdi-panda',
  'mdi-owl',
  'mdi-pig-variant',
  'mdi-pig-variant-outline',
  'mdi-rabbit',
  'mdi-shark',
  'mdi-sheep',
  'mdi-snail',
  'mdi-snake',
  'mdi-spider',
  'mdi-spider-outline',
  'mdi-tortoise',
  'mdi-turkey',
  'mdi-turtle',
  'mdi-unicorn',
  'mdi-unicorn-variant',
  // plants
  'mdi-barley',
  'mdi-carrot',
  'mdi-chili-mild',
  'mdi-chili-mild-outline',
  'mdi-corn',
  'mdi-flower',
  'mdi-flower-poppy',
  'mdi-flower-outline',
  'mdi-flower-tulip',
  'mdi-flower-tulip-outline',
  'mdi-food-apple',
  'mdi-food-apple-outline',
  'mdi-forest',
  'mdi-fruit-cherries',
  'mdi-fruit-grapes',
  'mdi-fruit-grapes-outline',
  'mdi-fruit-pineapple',
  'mdi-fruit-watermelon',
  'mdi-grain',
  'mdi-leaf',
  'mdi-mushroom',
  'mdi-mushroom-outline',
  'mdi-peanut',
  'mdi-peanut-outline',
  'mdi-pine-tree',
  'mdi-seed',
  'mdi-seed-outline',
  'mdi-sprout',
  'mdi-sprout-outline',
  'mdi-tree',
  'mdi-tree-outline',
  'mdi-waves',
  // weather
  'mdi-cloud',
  'mdi-cloud-outline',
  'mdi-weather-cloudy',
  'mdi-weather-dust',
  'mdi-weather-fog',
  'mdi-weather-hail',
  'mdi-weather-hazy',
  'mdi-weather-lightning',
  'mdi-weather-pouring',
  'mdi-weather-rainy',
  'mdi-windsock',
  // emojis
  'mdi-emoticon',
  'mdi-emoticon-outline',
  'mdi-emoticon-cool-outline',
  'mdi-emoticon-neutral',
  'mdi-emoticon-neutral-outline',
  'mdi-emoticon-wink',
  'mdi-emoticon-wink-outline',
  'mdi-sticker-emoji'
]
