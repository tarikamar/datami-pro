// --------------------
// REQUESTS
// --------------------

// BOTH DEBOUNCER METHODS ARE WORKING ...

export const debounce = (func: any, delay: number = 500) => {
  let timeoutId: ReturnType<typeof setTimeout>
  return (...args: any[]) => {
    // console.log('debounce > delay : ', delay)
    // console.log('debounce > timeoutId : ', timeoutId)
    // console.log('debounce > func : ', func)
    // console.log('debounce > args : ', args)
    clearTimeout(timeoutId)
    timeoutId = setTimeout(() => {
      func.apply(this, args)
    }, delay)
  }
}

// export function debouncer<T extends (...args: any[]) => void>(fn: T, delay: number = 500): T {
//   let timeoutID: ReturnType<typeof setTimeout>
//   return function (this: any, ...args: any[]) {
//     console.log('debounce > delay : ', delay)
//     clearTimeout(timeoutID)
//     timeoutID = setTimeout(() => {
//       fn.apply(this, args)
//     }, delay)
//   } as T
// }
