export default defineAppConfig({
  appName: 'Datami PRO',
  appLogoUrl: 'https://raw.githubusercontent.com/multi-coop/datami-website-content/main/images/logos/logo-DATAMI-rect-colors-03.png'
})
