import { resolve, dirname } from 'node:path'
import { fileURLToPath } from 'url'
import VueI18nVitePlugin from '@intlify/unplugin-vue-i18n/vite'
import vuetify, { transformAssetUrls } from 'vite-plugin-vuetify'

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    '@pinia/nuxt', //
    '@nuxtjs/i18n',
    '@nuxtjs/eslint-module',
    // '@invictus.codes/nuxt-vuetify',
    '@sidebase/nuxt-auth',
    // '@nuxtjs/ngrok',
    (_options, nuxt) => {
      nuxt.hooks.hook('vite:extendConfig', (config) => {
        // @ts-expect-error
        config.plugins.push(vuetify({ autoImport: true }))
      })
    }
  ],
  typescript: {
    typeCheck: true,
    strict: true,
    shim: false
  },
  routeRules: {
    '/subscribe/checkout': { ssr: false }
  },
  // imports: {
  //   presets: [
  //     {
  //       from: 'vue-i18n',
  //       imports: ['useI18n']
  //     }
  //   ]
  // },
  // imports: {
  //   dirs: [
  //     'stores'
  //     // 'components/**', //
  //     // 'node_modules/h3/', //
  //     // 'types', //
  //     // 'types/*.ts', //
  //     // 'server/utils/**',
  //     // 'server/database/**'
  //   ]
  // },
  app: {
    head: {
      title: process.env.APP_NAME || 'Datami PRO',
      link: [{ rel: 'icon', type: 'image/png', href: '/logo-DATAMI-rect-colors-03.png' }],
      script: [{ src: 'https://js.stripe.com/v3' }]
    }
  },

  // SIBEBASE NUXT-AUTHH OPTIONS
  auth: {
    baseURL: process.env.AUTH_ORIGIN,
    provider: {
      type: 'authjs'
    },
    globalAppMiddleware: true
  },
  runtimeConfig: {
    AUTH_SECRET: process.env.AUTH_SECRET,
    API_ROUTE_SECRET: process.env.API_ROUTE_SECRET,

    GITHUB_CLIENT_SECRET: process.env.GITHUB_CLIENT_SECRET,
    GITLAB_CLIENT_SECRET: process.env.GITLAB_CLIENT_SECRET,

    EMAIL_SERVER_USER: process.env.EMAIL_SERVER_USER,
    EMAIL_SERVER_PASSWORD: process.env.EMAIL_SERVER_PASSWORD,
    EMAIL_SERVER_HOST: process.env.EMAIL_SERVER_HOST,
    EMAIL_SERVER_PORT: process.env.EMAIL_SERVER_PORT,
    EMAIL_FROM: process.env.EMAIL_FROM,
    STRIPE_SECRET_KEY: process.env.STRIPE_SECRET_KEY,

    public: {
      APP_NAME: process.env.APP_NAME,
      APP_DOMAIN: process.env.APP_DOMAIN,

      GITHUB_CLIENT_ID: process.env.GITHUB_CLIENT_ID,
      GITLAB_CLIENT_ID: process.env.GITLAB_CLIENT_ID,

      STRIPE_PUBLISHABLE_KEY: process.env.STRIPE_PUBLISHABLE_KEY
    }
  },

  // I18N OPTIONS
  i18n: {
    strategy: 'no_prefix',
    defaultLocale: 'fr',
    locales: [
      { code: 'en', name: 'English', iso: 'en-US' },
      { code: 'fr', name: 'Français', iso: 'fr-FR' }
    ], // used in URL path prefix
    vueI18n: './i18n.config.ts' // if you are using custom path, default
  },

  // VUETIFY OPTIONS
  // vuetify: {
  //   /* vuetify options */
  //   vuetifyOptions: {
  //     // @TODO: list all vuetify options
  //   },

  //   moduleOptions: {
  //     /* nuxt-vuetify module options */
  //     treeshaking: true,
  //     useIconCDN: true,

  //     /* vite-plugin-vuetify options */
  //     styles: true, // | 'none' | 'expose' | 'sass' | { configFile: string },
  //     autoImport: true,
  //     useVuetifyLabs: true // | false,
  //   }
  // },

  // PINIA OPTIONS
  pinia: {
    storesDirs: ['./stores/**']
  },

  // NGROK OPTIONS
  // ngrok: {
  //   activate: false,
  //   // activate: process.env.NGROK_ACTIVATE === 'true' || false,
  //   // domain: process.env.NGROK_DOMAIN || 'test-datami-pro', // only for Personal / Pro / Entreprises
  //   authtoken: process.env.NGROK_AUTHTOKEN,
  //   basic_auth: process.env.NGROK_BASIC_AUTH
  // },

  css: ['assets/css/main.css'],

  build: {
    transpile: [
      'vue-i18n', //
      'vuetify'
    ]
  },
  vite: {
    ssr: { noExternal: ['vuetify'] },
    vue: {
      template: {
        transformAssetUrls
      }
    },
    plugins: [
      VueI18nVitePlugin({
        include: [resolve(dirname(fileURLToPath(import.meta.url)), './locales/*.json')]
      })
    ]
  }
})
