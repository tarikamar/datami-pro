import { loadStripe } from '@stripe/stripe-js'
import type { Stripe } from '@stripe/stripe-js'
import { onMounted, useNuxtApp, useState } from '#imports'
/**
 * useClientStripe function
 *
 * This function is a helper to easily access the Stripe instance provided by the Nuxt plugin.
 * It can be used in components or pages to interact with the Stripe.js library.
 *
 * Based on : https://github.com/fuentesloic/nuxt-stripe/blob/main/src/runtime/composables/useClientStripe.ts
 */

/* eslint-disable require-await */
export default async function useClientStripe() {
  const nuxtApp: any = useNuxtApp()
  const stripe = useState<Stripe | null>('stripe-client', () => null)
  const isLoading = useState('stripe-client-loading', () => false)

  async function _loadStripe() {
    if (stripe.value) {
      return stripe.value
    }

    isLoading.value = true
    // console.log(nuxtApp.$config)
    if (!nuxtApp.$config.public.STRIPE_PUBLISHABLE_KEY) console.warn('no key given for client service')

    return await loadStripe(nuxtApp.$config.public.STRIPE_PUBLISHABLE_KEY, nuxtApp.$config.public.stripeOptions)
  }

  onMounted(async () => {
    if (!isLoading.value) {
      const _stripe = await _loadStripe()
      stripe.value = _stripe
      isLoading.value = false
    }
  })

  return stripe
}
