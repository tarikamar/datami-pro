// Composable to make authentication tasks easier
export default function useMyAuth() {
  return {
    register,
    // verifyEmail,
    // verifyEmailToken,
    // resetUserPassword,
    // verifyUserResetPassword,
    getProfile,
    updateProfile,
    getLastActiveSubscription,
    getSubscriptions,
    isSubscribed
  }
}

// --------------------------
// NEW USER
// --------------------------

async function register(user: NewUser) {
  // Attempt register
  // console.log(user)
  return await useFetch('/api/auth/register', {
    method: 'POST',
    body: user
  })
}

// --------------------------
// USER PROFILE
// --------------------------

async function getProfile() {
  return await $fetch('/api/user/profile')
}

async function updateProfile(user: UserData) {
  return await $fetch('/api/user/update', {
    method: 'PUT',
    body: user
  })
}

// --------------------------
// USER SUBSCRIPTIONS
// --------------------------

async function getLastActiveSubscription(user: BodyUser) {
  const res = await useFetch('/api/me/current-subscription', {
    method: 'POST',
    body: user
  })
  return res
}

async function getSubscriptions(user: BodyUser) {
  const res = await useFetch('/api/me/subscriptions', {
    method: 'POST',
    body: user
  })
  return res
}
async function isSubscribed(user: BodyUser) {
  // const { data, error } = await useFetch('/api/me/subscriptions', {
  //   method: 'POST',
  //   body: user
  // })
  const { data, error } = await getLastActiveSubscription(user)
  // console.log('data.value : ', data.value)
  if (error.value) {
    throw createError({
      statusCode: 500,
      statusMessage: 'Subscription error'
    })
  }
  const subscription: Subscription = data.value as Subscription
  const isActive = subscription?.stripeStatus === 'active'
  return isActive
}

// --------------------------
// SECURITY
// --------------------------

// async function verifyEmail(email: string) {
//   return await $fetch('/api/auth/verify-email', {
//     method: 'POST',
//     headers: {
//       'client-platform': 'browser'
//     },
//     body: {
//       email
//     }
//   })
// }

// async function verifyEmailToken(token: string) {
//   return await $fetch('/api/auth/verify-email-token', {
//     method: 'POST',
//     headers: {
//       'client-platform': 'browser'
//     },
//     body: {
//       token
//     }
//   })
// }

// async function resetUserPassword(email: string) {
//   return await $fetch('/api/auth/reset-password', {
//     method: 'POST',
//     headers: {
//       'client-platform': 'browser'
//     },
//     body: {
//       email
//     }
//   })
// }

// async function verifyUserResetPassword(token: string) {
//   return await $fetch('/api/auth/verify-reset-password', {
//     method: 'POST',
//     headers: {
//       'client-platform': 'browser'
//     },
//     body: {
//       token
//     }
//   })
// }
