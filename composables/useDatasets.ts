// Composable to make authentication tasks easier
export default function useDatasets() {
  return {
    getUserDatasets,
    createDataset,
    getDatasetById,
    updateDatasetById,
    deleteDatasetById
  }
}

// --------------------------
// USER DATASETS
// --------------------------

const getUserDatasets = async (user: BodyUser) => {
  // console.log('useMyAuth > getUserDatasets > lazy : ', lazy)

  const url = '/api/datasets/my-datasets'
  const requestKey = ref('my-datasets')
  // const res = await useFetch(url, {
  //   method: 'POST',
  //   body: user,
  //   lazy
  // })
  // return res
  const {
    data, //
    pending,
    status,
    refresh,
    error
  } = await useLazyFetch(url, {
    method: 'POST',
    body: user,
    immediate: true,
    key: `${requestKey.value}`,
    server: true
  })

  return { data, pending, status, refresh, error, requestKey }
}

const createDataset = async (body: BodyCreateDataset, lazy: boolean = false) => {
  const res = await useFetch('/api/dataset/create', {
    method: 'POST',
    body,
    lazy
  })
  return res
}

const getDatasetById = async (user: BodyUser, datasetId: string) => {
  const url = `/api/dataset/${datasetId}`
  const requestKey = ref(`dataset-${datasetId}`)

  // const res = await useFetch(url, {
  //   method: 'POST',
  //   body: user,
  //   key: datasetId,
  //   lazy: false
  // })
  // // console.log('useMyAuth > getDatasetById > res : ', res)
  // // console.log('useMyAuth > getDatasetById > res.value : ', res.value)
  // return res

  const {
    data, //
    pending,
    status,
    refresh,
    error
  } = await useLazyFetch(url, {
    method: 'POST',
    body: user,
    key: `${requestKey.value}`,
    immediate: true,
    // lazy: false,
    server: false
  })

  return { data, pending, status, refresh, error, requestKey }
}

const updateDatasetById = async (user: BodyUser, datasetId: string, data: Dataset) => {
  const res = { user, datasetId, data }
  console.log('updateDatasetById > res :', res)
  // const res = await useFetch(`/api/dataset/${datasetId}`, {
  //   method: 'PUT',
  //   body: user
  // })
  return res
}

const deleteDatasetById = async (user: BodyUser, datasetId: string) => {
  const res = await useFetch('/api/dataset/delete', {
    method: 'DELETE',
    body: {
      userId: user.userId,
      datasetId
    }
  })
  return res
}
